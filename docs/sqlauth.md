# Setting SQL Auth in SQLExpress

## SSMS

_Insert screenshot later_

## CLI

1. Login to the the SQLExpress instance with your Windows credentials:
`sqlcmd /S fortify-wi-test\sqlexpress /E`
2. Enable sa account

    ```sqlcmd
    ALTER LOGIN [sa] WITH PASSWORD='SAPassword', CHECK_POLICY=OFF
    GO
    ALTER LOGIN [sa] ENABLE
    GO
    ```

3. Enable Mixed Mode Authentication (1 = Windows Auth only, 2 = Mixed Mode):

    ```sqlcmd
    USE [master]
    GO
    EXEC xp_instance_regwrite N'HKEY_LOCAL_MACHINE',
        N'Software\Microsoft\MSSQLServer\MSSQLServer',
        N'LoginMode', REG_DWORD, 2
    GO
    ```

4. Exit SQL Connection
`exit`
5. Restart SQLExpress instance
  `net stop MSSQL$SQLEXPRESS`
  followed by
  `net start MSSQL$SQLEXPRESS`
6. Login with your sa account to verify
`sqlcmd /S .\sqlexpress -U sa -P SAPassword`
7. Another way to verify is to execute the following command (1 = Windows Auth only, 0 = Mixed Mode)
`SELECT SERVERPROPERTY('IsIntegratedSecurityOnly');`
