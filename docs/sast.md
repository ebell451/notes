# [Fortify Static](https://www.microfocus.com/en-us/solutions/application-security)

## [Community](https://community.microfocus.com/t5/Fortify/ct-p/fortify)

## [Fortify Unplugged](https://www.youtube.com/c/FortifyUnplugged) YouTube Channel

## [SSC - Software Security Center](https://www.microfocus.com/en-us/products/software-security-assurance-sdlc/overview)

## [Documentation](https://www.microfocus.com/documentation/fortify-software-security-center/)

## [SCA - Static Code Analyzer](https://www.microfocus.com/en-us/products/static-code-analysis-sast/overview)

## [Documentation](https://www.microfocus.com/documentation/fortify-static-code-analyzer-and-tools/)

### Setting up JAVA user environment variables

Set the following user environment variables (== environment variables of type user variables)

```text
• JAVA_HOME : C:\Program Files\Java\jdk1.8.0_112
• JDK_HOME : %JAVA_HOME%
• JRE_HOME : %JAVA_HOME%\jre
• CLASSPATH : .;%JAVA_HOME%\lib;%JAVA_HOME%\jre\lib
• PATH : your-unique-entries;%JAVA_HOME%\bin
```

NOTE: make sure that the longish your-unique-entries does not contain any other references to another Java installation folder.

#### Build Jenkins Plugin

Steps on how to create the SCA Jenkins plugin 19.1.0 from github

1. Download Fortify Jenkins plugin zip file from <https://github.com/jenkinsci/fortify-plugin>

2. setup environment
   - install maven and make it accessible via PATH system environment variable (`mvn -version`)
   - install JDK  and make accessible via PATH system environment variable (`java -version` or `javac -version`)

```bash
set JAVA_HOME=path\JDK_path
set MAVEN_OPTS=-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=5005 (as per the github setting)
set PATH=path\maven\bin;path\JDK\bin;%path%
```

3. download certificates from <https://repo.jenkins-ci.org> (download/export cert using the lock icon in the browser's URL bar) or you will get the following error.
eg
mvn clean
Listening for transport dt_socket at address: 5005
[INFO] Scanning for projects...
Downloading: <https://repo.jenkins-ci.org/public/org/jenkins-ci/plugins/plugin/3.4/plugin-3.4.pom>
Downloading: <https://dl.bintray.com/fortify/maven-public/org/jenkins-ci/plugins/plugin/3.4/plugin-3.4.pom>
Downloading: <https://repo.maven.apache.org/maven2/org/jenkins-ci/plugins/plugin/3.4/plugin-3.4.pom>
[ERROR] [ERROR] Some problems were encountered while processing the POMs:
[FATAL] Non-resolvable parent POM for org.jenkins-ci.plugins:fortify:19.1.30-SNAPSHOT: Could not transfer artifact org.jenkins-ci.plugins:plugin:pom:3.4 from/to repo.jenkins-ci.org (<https://repo.jenkins-ci.org/public/>): sun.security.validator.ValidatorException: PKIX path building failed: sun.security.provider.certpath.SunCertPathBuilderException: unable to find valid certification path to requested target and 'parent.relativePath' points at wrong local POM @ line 19, column 13

4. add https://repo.jenkins-ci.org cert to JDK's keystore
eg
C:\_Java\jdk1.8.0_191\bin\keytool.exe -importcert -trustcacerts -noprompt -storepass changeit -keystore C:\_Java\jdk1.8.0_191\jre\lib\security\cacerts -file ..\repo_jenkins-ci_org.crt -alias repo.jenkins-ci.org
Certificate was added to keystore

4. download https://dl.bintray.com/fortify/maven-public/com/fortify/ssc-restapi-client/2.0/ssc-restapi-client-2.0.jar and place it under the following directory
eg
C:\Users\<username>\.m2\repository\com\fortify\ssc-restapi-client\2.0

5. run the following

```bash
cd fortify-plugin-master
mvn clean
mvn package (this will create a target subfolder where fortify.hpi will be created)
```

6. install 19.1.0 Fortify Jenkins plugin

Pipeline SCA Jenkins plugin example

```bash
pipeline {
  agent any
    environment {
        PATH="C:\\_FORTIFY\\Fortify_SCA_and_Apps_19.1.0\\bin;%PATH%"
    }
    stages {
        stage('fortify clean') {
          steps{
            echo 'Cleaning'
            fortifyClean buildID: 'EightBall'
          }
        }
        stage('fortify Translate') {
      steps{
        echo 'Translating'
        fortifyTranslate buildID: 'EightBall',
        projectScanType: fortifyJava(javaSrcFiles: 'C:\\_FORTIFY\\Fortify_SCA_and_Apps_19.1.0\\Samples\\basic\\eightball\\EightBall.java', javaVersion: '1.8')
      }
    }
        stage('Deploy') {
    steps{
        echo 'Scanning'
        fortifyScan buildID: 'EightBall', resultsFile: 'EightBall.fpr'
    }
        }
    }
}
```

#### Install Maven Plugin

In the /plugins/maven/ directory extract the maven-plugin-srv file. Open a command prompt or terminal window. Browse to the /plugins/mavin/maven-plugin-src directory and execute the following command as mentioned in the README:

`mvn clean install`

After installing the plugin there are three commands that need to be executed your maven project directory (where the pom.xml file is located) - clean, translate, scan.

1. ```mvn clean```
2. ```mvn com.fortify.sca.plugins.maven:sca-maven-plugin:clean```
3. ```mvn com.fortify.sca.plugins.maven:sca-maven-plugin:translate```
4. ```mvn com.fortify.sca.plugins.maven:sca-maven-plugin:scan -Dfortify.sca.verbose=true -Dfortify.sca.debug=true``` - the switches are optional but recommended

After executing the above, you will have an fpr file created in the target/fortify directory

macOS Maven

- Local Repository: /Users/<username>/.m2/repository
