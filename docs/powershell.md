# PowerShell Commands and Information

## Get external IP Address

`(Resolve-DnsName -Name myip.opendns.com -Server 208.67.222.220).IPAddress`

## Viewing history of commands executed (PowerShell7)

You can obtain a list of prior commands executed with `Get-History`. However, this only provides commands executed during the current session.

With that in mind, PowerShell does store all commands in a text file. You can obtain the location of the test file with:

`(Get-PSReadlineOption).HistorySavePath`

You can view the contents of this file by appending `Get-Content` at the beginning and winding up with:

`Get-Content (Get-PSReadlineOption).HistorySavePath`

I came across some code to create a function allowing you to search this file:

```PoSH
function hist {
  $find = $args;
  Write-Host "Finding in full history using {`$_ -like `"*$find*`"}";
  Get-Content (Get-PSReadlineOption).HistorySavePath | ? { $_ -like "*$find*" } | Get-Unique | more
}
```

To clear the contents of the file, you can run:

`Remove-Item (Get-PSReadlineOption).HistorySavePath`

## Wipe all Windows event logs

`wevtutil el | Foreach-Object {Write-Host "Clearing $_"; wevtutil cl "$_"}`

## Dump all Windows Services

`$service = get-wmiobject -query 'select * from win32_service'; echo $service.pathname |select-string -Pattern 'system32|c:\windows\system32|TrustedInstaller|SysWow64' -NotMatch | export-csv "$env:\Support\out.csv" -nti |start "$env:\Support\out.csv"`

## WMIC Commands

### The WMIC utility was depracated in Windows 10 21H1

Starting with Windows 11 build 22572, you can add this back as an optional feature`

Add WMIC
`DISM /Online /Add-Capability /CapabilityName:WMIC~~~~`

Remove WMIC
`DISM /Online /Remove-Capability /CapabilityName:WMIC~~~~​`

- Spot Odd Executables - `wmic PROCESS WHERE "NOT ExecutablePath LIKE '%Windows%'" GET ExecutablePath`
- Look at services that are set to start automatically - `wmic SERVICE WHERE StartMode="Auto" GET Name,State`
- Find user-created shares (usually not hidden) - `wmic SHARE WHERE "NOT Name LIKE '%$'" GET Name,Path`
- Find stuff that starts on boot - `wmic STARTUP GET Caption,Command,User`
- Identify any local system accounts that are enabled (guest, etc.) - `wmic USERACCOUNT WHERE "Disabled=0 AND LocalAccount=1" GET Name"`
- Change Start Mode of Service - `wmic service where (name like "Fax" OR name like "Alerter") CALL ChangeStartMode Disabled`
- Number of Logons Per USERID - `wmic netlogin where (name like "%skodo") get numberoflogons`
- Obtain a Certain Kind of Event from Eventlog - `wmic ntevent where (message like "%logon%") list brief`
- Clear the Eventlog (Security example) - `wmic nteventlog where (description like "%secevent%") call cleareventlog`
- Get Mac Address - `wmic nic get macaddress`
- Reboot or Shutdown - `wmic os where buildnumber="2600" call reboot`
- Update static IP address - `wmic nicconfig where index=9 call enablestatic("192.168.16.4"), ("255.255.255.0")`
- Change network gateway - `wmic nicconfig where index=9 call setgateways("192.168.16.4", "192.168.16.5"),(1,2)`
- Enable DHCP - `wmic nicconfig where index=9 call enabledhcp`
- Service Management - `wmic service where caption="DHCP Client" call changestartmode "Disabled"`
- Start an Application - `wmic process call create "calc.exe"`
- Terminate an Application - `wmic process where name="calc.exe" call terminate`
- Change Process Priority - `wmic process where name="explorer.exe" call setpriority 64`
- Get List of Process Identifiers - `wmic process where (Name='svchost.exe') get name,processid`
- Information About Harddrives - `wmic logicaldisk where drivetype=3 get name,freespace,systemname,filesystem,size,volumeserialnumber`
- Information about os - `wmic os get bootdevice,buildnumber,caption,freespaceinpagingfiles,installdate,name,systemdrive,windowsdirectory /format:htable > c:osinfo.htm`
- Information about files - `wmic path cim_datafile where "Path='windowssystem32wbem' and FileSize>1784088" > c:wbemfiles.txt`
- Process list - `wmic process get /format:htable > c:process.htm`
- Retrieve list of warning and error events not from system or security logs - `wmic NTEVENT WHERE "EventType<3 AND LogFile != 'System' AND LogFile != 'Security'" GET LogFile,SourceName,EventType,Message,TimeGenerated /FORMAT:"htable.xsl":" datatype = number":" sortby = EventType" > c:appevent.htm`
- Total Hard Drive Space Check - `wmic logicaldisk list brief`
- Get Running Services Information - `wmic service where (state=”running”) get caption,name,startmode,state`
- Get Startmode of Services - `wmic service get caption,name,startmode,state`
- Get Domain Names And When Account PWD set to Expire - `wmic UserAccount GET name,PasswordExpires /Value`
- Get Hotfix and Security Patch Information - `wmic QFE GET /format:CSV >QFE.CSV`
- Get Startup List - `wmic startup list full`
- Find a specific Process - `wmic process list brief - find "cmd.exe"`
- Get List of IP Interfaces - `wmic nicconfig where IPEnabled='true'`
- Change IP Address - `wmic nicconfig where Index=1 call EnableStatic ("10.10.10.10"), ("255.255.255.0")`
- OS/System Report HTML Formatted - `wmic /output:c:os.html os get /format:hform`
- Products/Programs Installed Report HTML Formatted - `wmic /output:c:product.html product get /format:hform`
- Services Report on a Remote Machine HTML Formatted - `wmic /output:c:services.htm /node:server1 service list full / format:htable`
- Get Server Drive Space Usage Remotely - `wmic /Node:%%A LogicalDisk Where DriveType="3" Get DeviceID,FileSystem,FreeSpace,Size /Format:csv MORE /E +2 >> SRVSPACE.CSV`
- Get PC Serial Number - `wmic /node:”HOST” bios get serialnumber`
- Get PC Product Number - `wmic /node:”HOST” baseboard get product`
- Get Services for Remote Machine in HTML Format - `wmic /output:c:services.htm /node:server1 service list full / format:htable`

### RDP Issue Connecting to Server 2012R2/2016 in VMWare

1. Make sure you have PSExec on the machine to elevate the PowerShell CLI
2. On some systems we had to go a step further than the script below and give System account Full Control to the MachineKeys folder.
3. Open an elevated command prompt and execute the following one line at a time

```posh
psexec -i -s powershell.exe
cd C:\ProgramData\Microsoft\Crypto\RSA\MachineKeys
mkdir ..\Old_keys
Get-ChildItem -Path . | % {icacls $_.Fullname /grant "NT AUTHORITY\SYSTEM:F"}
Get-ChildItem -Path . | Move-Item -Destination ..\Old_keys
Restart-Computer
```
