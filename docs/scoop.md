# Scoop commands

```cli
powershell iwr -useb get.scoop.sh | iex
or
Invoke-Expression (New-Object System.Net.WebClient).DownloadString('https://get.scoop.sh')

scoop install sudo
sudo scoop install 7zip git openssh --global
scoop bucket add extras

Invoke-Expression (New-Object System.Net.WebClient).DownloadString('https://get.scoop.sh')
scoop bucket add extras
scoop bucket add java
sudo scoop install -g vscodium
sudo scoop install -g googlechrome


scoop update *
scoop update * -g

scoop cleanup-all
sudo scoop cleanup -g *
```
