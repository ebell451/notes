# Potential Information to Collect While Footprinting

## Network

- access control mechanisms and access control lists
- authentication mechanisms
- domain name(s)
- IDs
- internal domain names
- IP address of reachable systems
- network blocks
- networking protocols
- private websites
- rogue websites
- system enumeration
- TCP and UDP services running
- telephone numbers
- VPN devices

## Systems

- passwords
- remote system types
- routing tables
- SNMP information
- system architecture
- system banners
- system names
- user and group names

## Organization

- address and phone numbers
- background of the organization
- comments HTML source code
- company directory
- employee details
- location details
- news articles
- organization website(s)
- press releases
- security policies implemented
- web server links relevant to the organization