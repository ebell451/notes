# [How to **Acquire** ANY Language Not Learn IT!](https://www.youtube.com/watch?v=illApgaLgGA)

100 different teaching methods, but the best is to learn closest to how a baby acquires it's first language

## Comprehensible Input

- Natural Approach (1970's)
- TPRS (1992) - story telling / story listening

The language needs to be in 90% of the target language.

[Level of language learning](https://en.wikibooks.org/wiki/Wikibooks:Language_Learning_Difficulty_for_English_Speakers) as defined by State Department for English speakers

No reading or writing...only listening and speaking...especially for level 5

1. Need a language parent and/or partner (ask for 2 hours a week)
    - family
    - friends
    - co-workers
    - as a last resort you can look for trades / language exchanges
        - ESL classroom / center
        - Craigslist - "I'm acquiring this language would you like to trade?"
        - Apps
            - Tandem
            - hellotalk

2. The Magic
    Get as many magazines as you can friends (sunset magazine is a good one as it covers food, travel, etc and has tons of pictures)
    - Have them go through the magazine and label as many as the pictures as possible
    - What is this
    - What is that
    - What is he doing?
    - What is she doing?
    - Why
    As many children's stories you can find (pictures big and text small)
    - Do not translate the story. Retell it in the target language

    **listen, listen, listen**

    Three or four rules during the language session
    1. No English / stay in the target language as much as possible
        a. Use gestures or acting instead of English
        b. Draw
        c. If we can't determine or understand each other then in target language say "It's not important and we just move on.
    2. No grammar
    3. No correction at any time as it is a way of time

3. TPR = Total Physical Response (Acquiring a language through movement) / lot's of commands, verbs, actions
    - Give me a list of commands, verbs, action (eat, drink, jump, run, etc. ) 100-200 up to 500

4. Read - Read - Read

5. i+1 = input plus 1
    What you already know/possess then go slightly beyond
    loving explanation, not just describing

Rosetta Stone - super boring, missing the people, conversation, interaction - basically a digital flashcard + better than nothing
DuoLingo - basically memorization (goes to your short-term)

Use your mobile phone

---------------------

## [How to learn any language in 6 months](https://www.youtube.com/watch?v=d0yGdNEWdn0)

5 Principles / 7 Actions of Rapid Language Acquisition
To speak a language in 6 months - anyone can do it

(Two myths: talent and immersion)

Four words - these interconnect
    a. Attention
    b. Meaning
    c. Relevance
    d. Memory

### Five Principles

1. Focus on language content that is relevant to you
2. Use your new language as a tool to communicate...from day 1 (as a kid does)
3. Whe you first understand the message you will unconsciously **acquire**  the language. [Comprehensible Input]
4. It is about physiological training and not about knowledge
5. Psyco-physiological state matters

### Seven Actions

1. Listen a lot / brain soaking - liseten to the patterns and rhythyms
2. Focus on getting the meaning first (before the words)...for instance body language
3. Start mixing - get creative and start mixing
4. Focus on the core - high frequency content
   1. Start with the toolbox **all in the target language**
      1. What is this
      2. How do you say
      3. I don't understand
      4. Repeat that please
   2. Pronouns, common verbs
      1. you
      2. that
      3. give
      4. me
      5. hot
   3. Adjectives
   4. Glue words
      1. although
      2. but
      3. therefore
      4. and
5. Get a language parent
   1. work hard to understand what you mean
   2. never correct your mistakes
   3. feedback
   4. use words you know
6. Copy their face to get your muscles working right (hear how it sounds and look at a native speaker observe how they use their face)
7. "Direct connect" to mental images

---------------------

## [5 techniques / skills to speak any language](https://www.youtube.com/watch?v=-WLHr1_EVtQ)

1. Take a very deep breath and relax
   - Golden rule is make mistakes!
2. Scrap the foreign alphabet quit using your American filter
3. Find a stickler - someone that is detail oriented that will not let you get away with he mistakes - relationships - feel comfortable - encourage
4. Practice (shower conversations with yourself: both sides of conversation)
   - similar to visualization
5. Use language: find a conversation buddy formula
   - your target language should be your best language in common
   - or "secret language" in common (his example was German with his co-worker)
