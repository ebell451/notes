# Information related to Fortify WebInspect Enterprise

## [Documentation](https://www.microfocus.com/documentation/fortify-webinspect-enterprise/)

## [Community](https://community.microfocus.com/t5/Fortify/ct-p/fortify)

## [Fortify Unplugged](https://www.youtube.com/c/FortifyUnplugged) YouTube Channel

## Logs

- WIE Guide Scan Logs Location:

> C:\ProgramData\HP\WIE\Guided Scan\Logs

- WIE Thin Client log:

> C:\Users\<username>\AppData\Local\Microsoft\CLR_v4.0\UsageLogs\HP.ASC.WIE.GuidedSetup.exe.log

### Sensors

### Job Failing Due to Connection Failures - Requestor Settings

Customer mentioned the scan in WIE is failing. He mentioned the failure was inconsistent. Sometimes it would fail right after defining the job and sometimes it would fail after 20-30 minutes. They also mentioned other scans were completing without issue and that this issue was also happening on multiple sensors.

After reviewing the **AmpSensorWI_trace.log** (Located on Sensor - `*C:\ProgramData\HP\HP WebInspect\Amp\logs\AMPSensorWI_trace.log*`) noticed the following two errors:

A:

```log
    2019-06-07 13:18:18,985 INFO  [172] ScanManager.scannerMonitor_LogEvent - WebInspect log message (severity Error): Error:Connectivity issue, Reason:FirstRequestFailed, Server:<server>:8080, Error:(100)Unable to parse Web Server response : Unable to read data from the transport connection: An existing connection was forcibly closed by the remote host.:
    2019-06-07 13:18:19,001 INFO  [172] ScanManager.scannerMonitor_LogEvent - WebInspect log message (severity Info): Info:Stop Requested, reason=ConnectivityProblemEventHandlerArgs:
--> Reason:FirstRequestFailed
    Host:<server>
    Port:8080
    ServerTotalCount:1
    ServerConsequtiveCount:0
    ServerFirstFailureTime:6/7/2019 1:18:18 PM
    GlobalTotalCount:1
    GlobalConsequtiveCount:0
    GlobalFirstFailureTime:6/7/2019 1:18:18 PM
    StatusCode:0
    :ConnectivityProblemEventHandlerArgs:
```

Job stopped due to **FirstRequestFailed**. This can be addressed by unchecking the option for "If first request fails, stop scan."

B:

```log
    2019-06-14 12:36:42,078 INFO  [162] ScanManager.scannerMonitor_LogEvent - WebInspect log message (severity Info): Info:Stop Requested, reason=ConnectivityProblemEventHandlerArgs:
--> Reason:ServerConsecutive
    Host:<server>
    Port:8443
    ServerTotalCount:85
    ServerConsequtiveCount:74
    ServerFirstFailureTime:6/14/2019 12:30:17 PM
    GlobalTotalCount:75
    GlobalConsequtiveCount:74
    GlobalFirstFailureTime:6/14/2019 12:30:17 PM
    StatusCode:0
    :ConnectivityProblemEventHandlerArgs:
```

Job stopped as we reached the value for ServerConsecutive failures. This can be addressed by modifying two values: _Request timeout_ and _Consecutive 'single host' retry failures to stop scan_.
