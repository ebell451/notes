# MacOS CLI Commands

## Run OS X Updates from Terminal

### Install all available updates

`sudo softwareupdate -i -a`
**or**
`sudo softwareupdate -ia --verbose`

- i - install
- v - verbose mode
- a - all
- r - recommended

### List available updates

`softwareupdate -l`

### Install recommended updates

`sudo softwareupdate -ir --verbose`

## Change default location of screenshots

`defaults write com.apple.screencapture`

## Change default screenshot image type

`defaults write com.apple.screencapture type jpg && killall SystemUIServer`

## Create Mobile Account

`/system/library/coreservices/managedclient.app/contents/resources/createmobileaccount -n"UAID HERE"`

## Fix Corrupted plist File or Integrity Check

You can delete corrupt .plist files in order to fix them. To check integrity of plist files:

1. open Terminal (Application ->Utilities -> Terminal.app)
2. type `cd ~/Library/Preferences`
3. type `plutil org.mozilla.firefox.plist` (just as an example)

*plutil* will return **ok** or some kind of error depending on whether the plist is correct or not. You can also check out all plist files by typing:

`plutil *.plist`

the side effect is that you will be flooded with several ok responses so you will miss errors. The solution is the -s flag in order to see only detected errors:

`plutil -s *.plist`

## Force First Setup Wizard

Enter Terminal Mode

- Reboot
- Hold `command + S` to boot in single user mode
- When you get a text prompt enter the following commands to create a new admin account

```bash
mount -um /
rm /var/db/.AppleSetupDone
shutdown -h now
```

- After rebooting you should have a new admin account created when you login

## Hide all icons from the desktop

```bash
defaults write com.apple.finder CreateDesktop -bool false && killall Finder
```

## How to select text in QuickLook

- Activate

```bash
defaults write com.apple.finder QLEnableTextSelection -bool TRUE;killall Finder
```

- Deactivate

```bash
defaults delete com.apple.finder QLEnableTextSelection;killall Finder
```

## Show/Hide Hidden Files

To show hidden files type the following command (you can reverse it by changing TRUE to FALSE):

- Show hidden files:

```bash
defaults write com.apple.finder AppleShowAllFiles TRUE;killall Finder
```

- Hide hidden files:

```bash
defaults write com.apple.finder AppleShowAllFiles FALSE;killall Finder
```

## Solving WIFI Issues on Mac

<http://support.apple.com/kb/TS1398>

## Turn Off Safari Webpage Previews

Safari keeps a snapshot (webpage preview) of websites visited in `~/Library/Caches/com.apple.Safari/Webpage Previews/`

`defaults write com.apple.Safari DebugSnapshotsUpdatePolicy -int 2`

## Mac Package Managers

### HomeBrew

<http://brew.sh> - The missing package manger of OS X

### MacPorts

  What is currently installed - <http://www.macports.org> - <https://www.macports.org/ports.php> - <https://trac.macports.org/browser/trunk/dports>

```bash
port selfupdate
sudo port -v selfupdate
```

`sudo port upgrade outdated`

`port installed`

`port search htop`

`port info htop`

## Terminal Shells

- /bin/bash
- /bin/csh
- /bin/zsh
  - [Oh My Zsh](https://ohmyz.sh/) - Terminal Commands
    - tab - open the current directory in a new tab
    - pfd - return the path of the frontmost Finder window
    - pfs - return the current Finder selection
    - cdf - cd to the current Finder directory
    - pushdf - pushd to the current Finder directory
    - quick-look - Quick Look a specified file
    - man-preview - open a specified man page in Preview
    - trash - move a specified file to the Trash
- /bin/zsh-4.0.4 (Mac OS X 10.2.8 or earlier)
- /bin/zsh-4.1.1 (Mac OS X 10.3 or later)
- /bin/ksh (Mac OS X 10.4 or later)

## Show hidden files

`defaults write com.apple.finder AppleShowAllFiles TRUE`

## Create new admin account in Single-User Mode

1. Hold command + S to boot in single user mode
2. When you get a text prompt enter in these terminal commands to create a new admin account

```bash
mount -um /
rm /var/db/.AppleSetupDone
shutdown -h now
```

## Create mobile account

`/system/library/coreservices/managedclient.app/contents/resources/createmobileaccount -n"UAID HERE"`
