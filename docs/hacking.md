# Five Steps/Phases of Hacking

1.  Research/Reconnaissance/Footprinting - Intel - Information Gathering - Recon
    active passive - gather information from publicly accessible sources Know
    their security posture reduce focus area identify vulnerabilities document /
    draw network map

2.  Scanning the IP Address with nMap `nmap -A -T4 -p- 10.10.10.40` handshake
    syn syn ack reset rst (up arrow show stats) --other methodologies or
    strategies all scan 65,000 ports regular scan to see what ports are open
    then scan only the ports open

3.  Enumeration/Exploitation/Gain Access port 445 = SMB Metasploit use options
    set rhosts options targets exploit

4.  Post Exploitation / Maintain Access Keep or return to same level of access
    (rootkit, trojan) Manipulating data over a long period of time

5.  Cover Your Tracks do not be noticed overwrite, modify, destroy logs

\*\* message signing disabled on smb is a no-no, but is set by default, allows
man in the middle attacks <https://www.rapid7.com> is a good website

Capture 4-way handshake, then crack it. Test Guest network to see if it is
isolated <https://amzn.to/2XOLl0G>

What is the three-way handshake? TCP - SYN, SYN-ACK, ACK connected oriented,
sustainable, handshake (three-way) UDP - connectionless, fast, no handshake

Stealth scan, not as stealthy now, RST (reset). SYN, SYN-ACK, RST - don't
actually make a connection so it is supposed to be stealthy, but it can be
detected nowadays.

meterpreter

-   sysinfo

-   hashdump

-   getuid

-   shell

## Apps

-   metagoofil

-   web data extractor

-   octoparse

-   email tracker pro
