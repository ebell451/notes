# AUTOPSY TECHNICIAN (MORGUE SPECIALIST)

The Morgue Specialist position requires a high school diploma, though an additional background in mortuary or biological science is often beneficial. The morgue specialist position requires a great work ethic and the ability to work daily in some of the most challenging conditions of any career field. These individuals also require approximately six (6) months of on-the-job training before becoming independent. They assist the physicians with all aspects of the autopsy examination, morgue management, admission and release of bodies and evidence, photography, and radiography.

## Education and Experience

- Associate's degree with emphasis on the medical sciences such as chemistry or **biology**.
- Biological Science
- Mortoruary Science
