# Five Faith Filled Lessons Sports Can Teach Us

1. There is always hope.
2. Faith is perseverance. Everyone loves an underdog.
3. Faith honors rituals and traditions. Even though a ritual won't ensure a victory, it is a good reminder that something important is happening. Touching a rock, doing a dances, reciting a prayer may seem silly to an outsider, but its downright sacramental. Honor the rituals that give your life or program meaning.
4. Practice matters. Excellence is worth pursuing.
5. Miracles happen. Your hole in one is coming! Hail Mary's happen. So the next time you witness a three-pointer by a kid fresh off the bench, tell yourself, "All things are possible"!
