[![Documentation Status](https://readthedocs.org/projects/ebells-notebook/badge/?version=latest)](https://ebell.wiki/en/latest/?badge=latest)

# Welcome

Browse around to see if there is anything that is of use to you. I make no guarantees nor warrantees with the information contained herein. Use it or not...the choice is **always** yours.

## Purpose

The purpose of this site developed due to my need for accessing some notes while on a tech call with a customer. I had created some notes and queries on one of my work computers. When the time came and I needed them, I was unable to remote into that desktop. The reason the notes were not available was due to my daughters *accidentally* shutting the power to my remote access server. This forced me to place the customer(s) on hold and contact my buddy at the office to retrieve said notes and email them to me.

## Solution Chosen

This site is created using:

* [MkDocs](https://www.mkdocs.org) - the engine behind this whole thing
* [Read the Docs](https://www.readthedocs.org) - front-end
* [Bitbucket](https://www.bitbucket.org) - git repository
* [VSCodium](https://vscodium.github.io/) - not only used as an editor but also has extensions for viewing Markdown and source control. I can't say enough about using VSCodium and is an application I use daily.
* [Markdown](https://daringfireball.net/projects/markdown/) - language

MkDocs is really the driver behind this whole solution and what I initially found during my search. I didn't want to host it locally so the obvious solution would have been to go with GitHub; however, I'm already using GitHub to host another website for [recipes](http://recipes.bellfamilyonline.info).

This led me to seek another solution. While reading up on MKDocs they mentioned Read the Docs. That was one part, but I still needed a repository to host the files. That is when I discovered Read the Docs worked with additional git repositories. So I settled on Bitbucket.

I had already been using VSCodium as an editor and with GitHub for a while. I then discovered that Bitbucket could be added as an additional repository.

Regarding the decision to go with VSCodium, I moved from Notepad++ to Visual Studio Code for a number of different reasons. The main reason was the need for a cross-platform application that worked on WinOS, MacOS and Linux. While browsing the Internet and reading different blogs, I stumpled upon VSCodium. The benefit of VSCodium over Visual Studio code is VSCodium "releases VS Code without MS branding/telemetry/licensing".

## ~~Scoop~~ Shovel - Package Manager for Windows

~~[Scoop](http://scoop.sh/)~~ [Shovel](https://github.com/Ash258/Scoop-Core) is **the** package manager for Windows. Scoop is a command line package manager for windows that will let you easily download the tools you need/want. Open a powershell terminal and run:

```PowerShell
set-executionpolicy remotesigned -s cu

iex (new-object net.webclient).downloadstring('https://get.scoop.sh')
```
Scoop is designed to be utilized by non-admin users, if you are installing as an administrator or user with local admin rights on the box, then execute:

```PowerShell
iex "& {$(irm get.scoop.sh)} -RunAsAdmin"
```

If you receive an error stating "The request was aborted: Could not create SSL/TLS secure channel" run the following command
`[Net.ServicePointManager]::SecurityProtocol = [Net.ServicePointManager]::SecurityProtocol -bor [Net.SecurityProtocolType]::Tls12`

### Buckets

List of common scoop buckets I use:

* Extras bucket
  * `scoop bucket add extras`
* Allows the adding of some different fonts, specifically FiraCode
  * `scoop bucket add nerd-fonts`
* If you don't know what Nirsoft applications are then leave this of
  * `scoop bucket add nirsoft-alternative https://github.com/MCOfficer/scoop-nirsoft.git`
* Java
  * `scoop bucket add java`
* Same goes for Sysinternals, if you don't know what they are leave this off as well
  * `scoop bucket add Sysinternals 'https://github.com/Ash258/Scoop-Sysinternals.git'`
* Ash258 also has a bucket for other applications (i.e., FiddlerEverywhere)
  * `scoop bucket add Ash258 'https://github.com/Ash258/Scoop-Ash258.git'`
* This allows some alias commands in scoop
  * `scoop bucket add alias-additions 'https://github.com/snaphat/alias-additions.git'`
* This bucket is mainly for MinGW-W64 (64-bit GCC)
  * `scoop bucket add wangzq https://github.com/wangzq/scoop-bucket`

### Installs

List of common scoop installs I use...many of these are self-explanatory:

* `scoop install sudo aria2 git`
* `sudo scoop install 7zip openssh vscodium googlechrome fastcopy FiraCode github touch vlc --global`
* `scoop install win32-openssh`
* `scoop install alias-additions`

If you do not include the --global switch then applications are installed under the user's profile. When adding the --global switch, applications are installed in ProgramData for all users.

The sudo commands is similar to the *nix sudo command that allows you to install the applications with administrator rights. This is commonly used with the --global switch as you need admin rights to install applications to ProgramData.

### Updates - with Alias Additions

* `scoop update-all` - Update scoop and all applications
* `scoop update <appname>` - Update individual applications
* If you have apps installed globally, you will need to update them with `scoop update -g *` as the alias only updates apps installed in the current user process.

### The Power of Scoop - Scripting

Here is a script I run on all new VMs for work. I have a PowerShell script that executes to configure some settings as well as some other commands, but one thing I do is add a `scoop_installs.bat` to the RunOnce registry key for the current user. Here is what that bat file looks like:

```text
call scoop install sudo aria2 git
call scoop bucket add extras
call scoop bucket add nerd-fonts
call scoop bucket add Sysinternals 'https://github.com/Ash258/Scoop-Sysinternals.git'
call scoop bucket add alias-additions 'https://github.com/snaphat/alias-additions.git'
call scoop bucket add Ash258 'https://github.com/Ash258/Scoop-Ash258.git'
call scoop bucket add java
call scoop install vscodium FiraCode github touch grep logexpert notepadplusplus curl firefox geekuninstaller less postman fiddlereverywhere pshazz squirrel-sql telnet winscp openjdk alias-additions
```

Here is a list of applications I normally install (2020-02-03):
![alt text](https://bitbucket.org/ebell451/notes/raw/3075446303931098fab6254c1284e34ef8a41f84/docs/scoop_list.png "List of common applications I use")

This list is what I currently have installed (2022-10-16):
![alt text](https://bitbucket.org/ebell451/notes/raw/6cdbed90003ab9f20f8a9454aadecda9c0a3694a/docs/images/scoop_list_updated.png "Updated list of applications installed via Scoop")
