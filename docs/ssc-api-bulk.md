# Bulk creation of Projects / Project Versions in SSC via API

- The FortifyToken needs to be a *UnifiedLoginToken*
- *$i* was a variable used in a loop as I was running a script to automate the creation of thousands of them

## Create project

```bash
curl --insecure -v -i -H "Authorization: FortifyToken MmYwMDNhMDktZTgyZC00NTM0LTllMDQtMmZhYzVjMjg4NTJk" -H "Content-Type: application/json" -X POST --data '{"name":"1.0","description":"","active":true,"project":{"name":"Test'$i'","description":"","issueTemplateId":"Prioritized-HighRisk-Project-Template"},"issueTemplateId":"Prioritized-HighRisk-Project-Template","committed":true}' http://fortify-ssc/ssc/api/v1/projectVersions
```


## Modify the attributes

```bash
curl --insecure -v -i -H "Authorization: FortifyToken MmYwMDNhMDktZTgyZC00NTM0LTllMDQtMmZhYzVjMjg4NTJk " -H "Content-Type: application/json" -X POST --data '{
    "requests": [{"uri":"http://fortify-ssc/ssc/api/v1/projectVersions/'$i'/attributes",
   "httpVerb":"PUT",
   "postData":
   [
     {"attributeDefinitionId":5,"values":[{"guid":"Active"}],"value":null},
     {"attributeDefinitionId":1,"values":[{"guid":"High"}],"value":null},
     {"attributeDefinitionId":6,"values":[{"guid":"Partial"}],"value":null},
     {"attributeDefinitionId":7,"values":[{"guid":"externalpublicnetwork"}],"value":null}
   ]
  },
  {"uri":"http://fortify-ssc/ssc/api/v1/projectVersions/'$i'?hideProgress=true",
   "httpVerb":"PUT",
   "postData":{"committed":true}
  }
 ]
}' http://fortify-ssc/ssc/api/v1/bulk
```

## Uploaded fpr

```bash
curl -X POST "http://fortify-ssc/ssc/api/v1/projectVersions/$i/artifacts" -H "Authorization: FortifyToken MmYwMDNhMDktZTgyZC00NTM0LTllMDQtMmZhYzVjMjg4NTJk " -H "accept: application/json" -H "Content-Type: multipart/form-data" -F "file=@php.fpr"
```

## Approval or uploaded artifact

```bash
curl -X POST "http://fortify-ssc/ssc/api/v1/artifacts/action/approve" -H "Authorization: FortifyToken MmYwMDNhMDktZTgyZC00NTM0LTllMDQtMmZhYzVjMjg4NTJk " -H "accept: application/json" -H "Content-Type: multipart/form-data" --data '{"artifactIds":"'$i'","comment":"approved"}'
```

## Sample script

```bash
#!/bin/bash
for i in {50..2500..1}
do
  curl --insecure -v -i -H "Authorization: MmYwMDNhMDktZTgyZC00NTM0LTllMDQtMmZhYzVjMjg4NTJk" -X POST "http://fortify-ssc/ssc/api/v1/artifacts/action/approve" -H "accept: application/json" -H "Content-Type: application/json" --data '{"artifactIds":['$i'],"comment":"approved"}'
  pause 2
done
```
