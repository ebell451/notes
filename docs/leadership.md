# Leadership

1. Be Authenticate - Practice Rigorous Authenticity
   > "I was trusted because I was authentic."
2. Surrender the outcome
   > "I was effecient because I didn't obsess over outcomes I couldn't control."
3. Do uncomfortable work
   > "I was always willing to do the uncomfortable work."

>"So, I had Predictable results with integrity."

Good interview question to ask `What is your greatest weakness?` to see if the person presents an authentic answer.

## Leadership qualities

1. Confidence
   - to set a new vision
2. Courage
   - to believe something different, at least for that moment
3. Vulnerability
   - to speak this out loud
