# ScreenConnect / Connectwise Control Notes

The remote sharing server mentioned on the home page refers to ScreenConnect (SC) or what is now known as [Connectwise Control](https://www.connectwise.com/software/control). Here are some useful commands used in the command window.

**NOTE:** You will notice a couple of lines added to the scripts below that are specific commands for ScreenConnect. If you remove these two lines you can run these commands on any Windows machine. Here is an explanation of those lines:

- *#!ps* - This command tells SC the following commands are PowerShell commands. You are simply sending commands to the command prompt you can use *#!cmd* instead. I'm lazy and use *#!ps* for most of the commands.
- *#maxlength=100000* - This command defines the number of lines to be returned. You can modify this based on what you are expecting, again I simply use the same for all my commands.
- *#timeout=90000* - An important command that provides a timeout and is very useful for when you make a mistake! ;) Set this too low and your command may not complete...especially during installations.


## Create new domain user account from CLI

Create a new user account on the domain with full name, comment and require changing of password after initial logon:

```cli
net user username password /add /domain /fullname:"Full Name" /comment:"Position or Title" /logonpasswordchg:Yes
```

## Download a file using BITS

Here I am downloading an mst file matching the below msi file for installation.

```posh
#!ps
#maxlength=100000
#timeout=90000
bitsadmin  /transfer MEDCDownload /download  /priority normal http://www.thealliedteam.com/msp/agency/DesktopCentralAgent.mst C:\Support\DesktopCentralAgent.mst
```

## Another example of BITS download

What I'm actually doing here is copying the msi file matching the mst file above and then I will install the application.

```posh
#!ps
#maxlength=100000
#timeout=90000
bitsadmin /transfer MEDCDownload /download  /priority normal http://www.thealliedteam.com/msp/agency/DesktopCentralAgent.msi  C:\Support\DesktopCentralAgent.msi
```

## msiexec from cli to install the application

```posh
#!ps
#maxlength=100000
#timeout=90000
msiexec /i C:\Support\DesktopCentralAgent.msi TRANSFORMS="C:\Support\DesktopCentralAgent.mst" ENABLESILENT=yes REBOOT=ReallySuppress INSTALLSOURCE=Manual /lv C:\Support\Agentinstalllog-2.txt
```

## Downloading another file using BITS in Windows

This script contains commands for [Windows 10 Debloate](https://github.com/W4RH4WK/Debloat-Windows-10)

```posh
#!ps
#maxlength=100000
#timeout=90000
bitsadmin /transfer ScriptsDownload /download /priority normal http://www.thealliedteam.com/msp/scripts.zip  C:\Support\scripts.zip
```

## Yet another BITS download for unzip.exe hosted on my site

```posh
#!ps
#maxlength=100000
#timeout=90000
bitsadmin  /transfer ToolDownload  /download  /priority normal  http://www.thealliedteam.com/msp/unzip.exe  C:\Support\unzip.exe
```

## Combining and executing

Here I combine the commands to execute

```posh
bitsadmin /transfer ScriptsDownload /download  /priority normal http://www.thealliedteam.com/msp/scripts.zip  C:\Support\scripts.zip
bitsadmin /transfer ToolDownload /download /priority normal http://www.thealliedteam.com/msp/unzip.exe C:\Support\unzip.exe
c:\Support\unzip.exe C:\Support\scripts.zip -d C:\Support\
powershell Set-ExecutionPolicy Unrestricted
powershell c:\Support\scripts\optimize-windows-update.ps1
powershell Set-ExecutionPolicy Restricted
```

## Uninstall NinjaRMM

I was using NinjaRMM, but needed to uninstall. I achieved that with the following commands.

### First check to see if it is installed

`wmic product where "Name like '%%Ninja%%'" get name`

### Perform the un-installation/removal

```posh
#!ps
#maxlength=10000
#timeout=100000
wmic product where name="Ninja NMS" call uninstall /nointeractive
```

## Rename computer from CLI

`WMIC computersystem where caption='DESKTOP-95PQTKT' rename NEWNAME`

## Change from default WORKGROUP to a new one via CLI

`WMIC ComputerSystem Where Name="%NEWNAME%" Call JoinDomainOrWorkgroup Name="WORKGROUP-NEW"`

## Example of how to download a Windows update file using BITS inside of PS and then execute

```posh
#!ps
#maxlength=100000
#timeout=90000
bitsadmin  /transfer ScriptsDownload  /download  /priority normal  http://download.windowsupdate.com/d/msdownload/update/software/crup/2018/01/windows10.0-kb4074608-x64_1f59742e7b3cbd6ec6e1853869fa91576400bbcc.msu  C:\Support\windows10.0-kb4074608-x64_1f59742e7b3cbd6ec6e1853869fa91576400bbcc.msu
```

```cli
#!cmd
#maxlength=100000
#timeout=90000
wusa.exe C:\Support\windows10.0-kb4074608-x64_1f59742e7b3cbd6ec6e1853869fa91576400bbcc.msu /quiet /norestart
```

## Another example to install Windows update from CLI

`dism /online /get-packages | findstr KB4074608`

## Check to see if Windows update has been installed on a system

`SYSTEMINFO.exe | findstr KB4074608`

## Download [PatchMyPC](https://patchmypc.net)

```cli
#!cmd
#maxlength=100000
#timeout=90000
bitsadmin  /transfer ScriptsDownload  /download  /priority normal  https://patchmypc.net/freeupdater/PatchMyPC.exe  C:\Support\PatchMyPC.exe
```

## Modify Registry Entry via PS

This entry is not display last user logged on:

```powershell
#!ps
#maxlength=100000
#timeout=90000
Set-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System' -Name dontdisplaylastusername -Value 1
```

## Installing WebRoot AV from CLI and assigning license

```cli
#!cmd
#maxlength=100000
#timeout=90000
msiexec /i http://anywhere.webrootcloudav.com/zerol/wsasme.msi GUILIC=XXXX-XXXX-XXXX-XXXX CMDLINE=SME,quiet /qn /l*v install.log
```

## Uninstall WebRoot AV from CLI

`"C:\Program Files\Webroot\WRSA.exe" -uninstall`
`"C:\Program Files (x86)\Webroot\WRSA.exe" -uninstall`

## Windows Defender on Win 10

### Turn off

Run the following command in PowerShell:

`Set-MpPreference -DisableRealtimeMonitoring $true`

### Enable Controlled Folder Access

`Set-MpPreference -EnableControlledFolderAccess Enabled`

## CAUTION: Take ownership and delete

```cli
@echo off
takeown /A /R /F %1
rd /s /q %1
```

## Running Windows Updates from PowerShell CLI

The first thing you will need to do is to manually Install the Windows Update module,

```posh
#!ps
#maxlength=10000
#timeout=100000
Install-Module PSWindowsUpdate
```

Next, you will need to connect to the Windows Update servers and download the updates, if any are needed

```posh
#!ps
#maxlength=10000
#timeout=100000
Get-WindowsUpdate
```

Last, use the following to install the Windows Updates downloaded to the computer

```posh
#!ps
#maxlength=10000
#timeout=100000
Install-WindowsUpdate -AcceptAll -MicrosoftUpdate -AutoReboot
```

If needed, you may need to register the update service with the following:

```posh
#!ps
#maxlength=10000
#timeout=100000
Add-WUServiceManager -ServiceID "7971f918-a847-4430-9279-4a52d1efe18d" -Confirm:$false
```

## Updating SSL Cert for Server

After adding your certificate to the Local Computer\User store, you need to ensure it says, "You have a private key that corresponds to this certificate." If it does not, you will receive an error similar to the following:

```text
SSL Certificate add failed, Error: 1312
A specified logon session does not exist. It may already have been terminated.
```

To resolve this error, "click the Details tab and obtain the Serial number for the certificate. Then at a command prompt run the following:

```cli
certutil -repairstore my "<serial_number>"
```

To bind the certificate to the appropriate port, obtain the thumbprint of the certificate and run the following command, the appid for SC is irrelevant and you can use an empty GUID as mentioned:

```cli
netsh http add sslcert ipport=0.0.0.0:443 certhash=<thumbprint> appid="{00000000-0000-0000-0000-000000000000}"
```
