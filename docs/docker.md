# Using Docker Engine

## Installation

How to install the Docker Engine versus Docker Desktop on Windows 10/11. The ideal solution would be to install Docker Enterprise on Windows Server.

- PowerShell
  ```
  curl.exe -o docker.zip -LO https://download.docker.com/win/static/stable/x86_64/docker-20.10.12.zip
  Expand-Archive docker.zip -DestinationPath C:\
  [Environment]::SetEnvironmentVariable("Path", "$($env:path);C:\docker", [System.EnvironmentVariableTarget]::Machine)
  $env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine")
  dockerd --register-service
  Start-Service docker
  docker run hello-world
  ```

- Scoop - `scoop install main/docker`
- Chocolately - `choco install docker-engine`

## Additional Installs

Two of the main complaints of using the Docker Engine versus Docker Desktop is the availability of *docker scan* and *docker compose*. You can manually install both of these services:

- Docker Scan: https://github.com/docker/scan-cli-plugin#install-docker-scan

- Docker-Compose: https://docs.docker.com/compose/install/ [choose the option for Windows Server even though you will be installing on Win 10/11]

- You will also have to install Snyk CLI manually - https://docs.snyk.io/features/snyk-cli/install-the-snyk-cli