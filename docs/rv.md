# RV Camping

This page contains some of my notes and research for scheduling overnight camping trips.

## Websites

- [Allstays](https://www.allstays.com/) - Free guides online
- [Frugal RV Travel](https://www.frugal-rv-travel.com)
- [Creative RV](https://www.creativityrv.com/)

## Boondocking

We have stayed overnight at the following locations:

- [Walmart](https://www.walmartlocator.com/rv-parking-at-walmart/) - be sure to do your research or call the location as some do not allow this
- [Rest Areas](https://www.interstaterestareas.com/)

## Lakes around Houston, TX

### Lake Conroe

Lake Conroe is known for two things: partying and fishing, at least on the South side of the lake.

- Bishop's landing - Memberships / Coast-to-Cost (936) 856-2949 - Have to be a member; however, they do not sell memberships. Appears they have changed ownership and things have changed.
- Stow A Way Marina & RV Park - 936-856-4531 - no answer on Sunday
  - North Lake Paddling - 936-203-2697 - no answer, but received a text message stating they are "closed" until July 2. Responsive and friendly.
- [Park on the Lake](https://parkonthelake.com/)

### Lake Livingston

- Marina Village RV Resort - 936-594-0149

-------------------------------

## Memberships

- [Escapees](https://www.escapees.com/benefits/rv-parking/discount-park-directory/maps/) - Based out of Livingston, TX.
- [Passport America](https://www.passport-america.com/campgrounds/participating-campgrounds) - Good for Discounts on daily rates
- [Coast Resorts](https://www.coastresorts.com/directory/)
