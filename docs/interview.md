# Interview Questions and Resources

## Resources

- [Educba](https://www.educba.com/resources/)
  - [Web Development](https://www.educba.com/web-development-interview-questions/)
  - [Web Services](https://www.educba.com/web-services-interview-questions/)
  - [ITIL](https://www.educba.com/itil-interview-questions/)
  - [Pentration Testing](https://www.educba.com/penetration-testing-interview-questions/)
  - [JDBC](https://www.educba.com/jdbc-interview-questions/)
  - [Web Technologies](https://www.educba.com/web-technology-interview-questions/)
  - [IT Security](https://www.educba.com/it-security-interview-questions/)
  - [Ethical Hacking](https://www.educba.com/ethical-hacking-interview-questions/)
- [14 Revealing Interview Questions](https://www.inc.com/jeff-haden/14-revealing-interview-questions.html)

## Questions

- What online resources do you use to help you do your job?
- How do you keep your technology skills current?
- If you got hired, loved everything about this job, and were paid the salary you asked for, what kind of offer from another company would you consider?
- Name different types of Hackers. What is the Difference Between Black, White and Grey Hat Hacking? Red Team and Blue Team?
- What was the last presentation you gave?
- Windows, Linux or macOS?
- What are your favorite and least favorite technology products? Why?
- Why did you apply to our company? / What do you know about our company?
- What makes you qualified for this job?

## General TS and CS

- What made you get into technical support?
- What is your troubleshooting process?

  >- From CompTIA for A+ Cert Exams:
  >   - Identify the problem
  >   - Establish a theory of probable cause
  >   - Test the theory to determine cause
  >   - Establish a plan of action to resolve the problem and implement the solution
  >   - Verify full system functionality and if applicable implement preventative measures
  >   - Document findings, actions, and outcomes
  >- From Experience
  >   - Understand what the problem is - exactly
  >   - Gather as much information as possible about the problem
  >   - Eliminate variables, narrow it down, divide and conquer
  >   - Reproduce the problem and develop hypothesis of root cause
  >   - Attempt a workaround or fix
  >   - Problem solved? Record it for future issues [Technically the  documentation of the issue and steps to resolution should be made during the whole process.]

- What is the goal of technical support?
- What does Customer Service mean to you?
- What's one time you worked with a difficult customer and how did you resolve the situation?
- Tell me about a time you assisted a customer who'd worked with multiple agents and didn't get the help they needed.
- What is your preferred method of communication?
- Describe a time when you went above and beyond to help a customer.
- Which is more important: technical knowledge or customer service?
- In five minutes, could you explain something to me that is complicated but you know well?
- Tell me about the time you messed up.
- If you woke up and had 2,000 unread emails and could only answer 300 of them how would you choose which ones to answer?
- Who is the smartest person you know? Why?
- At your last job, what made you excited to get up and come to work?
- Is there anything about this interview process you would change?
- What qualities do you think are most important for this role?
- What is your favorite non-professional activity?

### What is the goal of technical support

> The goal of technical support is to provide help and advice to customers about products. You must understand how applicants go about solving customer issues because it's crucial to customer retention and business success.

What to look for in an answer:

- The candidate goes through the troubleshooting process correctly

  >- fact gathering
  >- diagnosis
  >- solutions
  >- test

- The candidate has a thorough yet adaptable troubleshooting process
- The candidate emphasizes solving customer needs as quickly and effectively as possible.

>_Example:_ "I first gather info and facts from the customer, then identify and verify the issue. Next, I try common quick fixes. If that's not successful, I use additional resources to research the issue. Once a solution is found, I perform appropriate repairs or replacement if necessary. After the solution is implemented, I test thoroughly to ensure everything is OK."

## Technical Interview Questions

- Database
  - What does CRUD stand for?\
  `Create, Read, Update, Delete`
  - In Microsoft SQL Server, what is the basic syntax for reading all the data from a table called information?\
  `select * from INFORMATION`
  - If you wanted to remove the contents of a table, what command would you use?\
   `truncate`
- Pretend I'm not a tech person. Can you explain [a relevant technology] in simple terms?
- "As much as you know, what happens when you type google.com into your browser and press enter"
- What is the difference between a proxy and a gateway?
  >Strictly speaking, proxies connect two or more applications that speak the same protocol, while gateways hook up two or more parties that speak different protocols. A gateway acts as a “protocol converter,” allowing a client to complete a transaction with a server, even when the client and server speak different protocols. </br></br>
  >In practice, the difference between proxies and gateways is blurry.Because browsers and servers implement different versions of HTTP,proxies often do some amount of protocol conversion. And commercialproxy servers implement gateway functionality to support SSL security protocols, SOCKS firewalls, FTP access, and web-based applications.
- [Can you explain XSS?](https://owasp.org/www-community/attacks/xss/)
  >Cross-Site Scripting (XSS) attacks are a type of injection, in which malicious scripts are injected into otherwise benign and trusted websites. XSS attacks occur when an attacker uses a web application to send malicious code, generally in the form of a browser side script, to a different end user.
- [What is OWASP?](https://owasp.org/about/)
  >The Open Web Application Security Project (OWASP) is a nonprofit foundation that works to improve the security of software.
- Can you name a few HTTP request methods or verbs? (CRUD Respectively)

  >- POST
  >- GET
  >- PUT
  >- DELETE
  >- PATCH

- What are some common status codes and their meaning?

  >- _2xx Series = Successful Response_
  >   - 200 OK
  >   - 201 Created
  >   - 202 Accepted
  >- _3xx Series = Redirection `Response_
  >   - 301 Moved Permanently
  >   - 302 Found
  >   - 304 Not Modified
  >- _4xx Series = Client Error_
  >   - 400 Bad Request
  >   - 401 Unauthorized
  >   - 403 Forbidden
  >   - 404 Not Found
  >   - 409 Conflict
  >- _5xx Series = Server Error_
  >   - 500 Internal Server Error
  >   - 502 Bad Gateway
  >   - 503 Service Unavailable

- What is the difference between a 400 series (client) and 500 series (server) error?
- What is the difference between a router and a switch?
  >a switch is designed to connect computers within a network, while a router is designed to connect multiple networks together
- In Linux you have the root account. What account would be the equivalent in Windows?

## AppSec Questions

- What is the difference between white box and black box testing?
  >Black Box = examining the functionality of an application
  >White Box = testing internal structures or workings of an application
- What is security testing?
  >  Security testing is a process intended to reveal flaws in the security mechanisms of an information system that protect data and maintain functionality as intended.
- What is a vulnerability?
  >weakness of any system through which intruders or bugs can affect attack on the system
- List the Seven Attributes of security testing:

  >- Authentication
  >- Authorization
  >- Confidentiality
  >- Availability
  >- Integrity
  >- Non-repudiation
  >- Resilience

- Which approach is better: a manual security test or an automated security test?

## Linux Job Interview Questions

- Check the kernel version `uname -a`
- See the current IP Adress `ifconfig or ip addr show eth0`
- Check for free disk space `df -ah`
- See if a Linux service is running `systemctl status <service_name>` or older system `service <service_name> status`
- Check the size of a directory `du`
- Check for open ports `netstat -tulpn` (what is the advantage of running that command as root - see PID/Program name)
- Check Linux CPU utilized by a process `ps aux | grep <service_name>` or `top`
- Hidden files in *nix are called "dot files".
  - How can you "hide" a file from a simple directory listing in *nix? Windows? `.filename` and `attrib +h`
  - At a command line, how can you view "hidden" files in *nix? Windows? `ls -a` `dir /a:h`
- How can you obtain a listing of hidden files and folders at the command line in Windows? `dir /a:h`
