# Synology Notes

## OpenVPN Server

There is a "bug" in OpenVPN where the service is not terminated. This manifests itself in at least two ways:

1. When you disable OpenVPN, then turn right around to enable it again. You will receive an error that the port is already in use.
2. When you update the certificate for the Syn, all other services pickup this change, but OpenVPN does not. What you need to do is:
   - SSH into the NAS
   - Terminate all active OpenVPN processes
          `killall -9 OpenVPN`
   - Restart the service
**or**
   - Uncheck OpenVPN in the GUI under VPN Server
   - Reboot the NAS
   - Go back and enable OpenVPN server

This resolves the error about an expired certificate even though the certificate has been renewed as alluded to above. You may have to export the new configuration after updating the security certificate.

## NextCloud

- Be sure to setup back-end http server for Apache instead of nginx. If you do not, the settings page will throw and error
- Here is a list of PHP extensions I had to enable to resolve errors and to configure the apps I wanted:
  - bcmath
  - bz2
  - curl
  - gd
  - gmp
  - imagick
  - intl
  - ldap
  - mysqli
  - openssl
  - pdo_mysql
  - posix - if this isn't added the security checkup on the overview page will not work
  - zip

### Imagick

If you receive a message about imagick, the simplest thing to do is turn off the Theming module.

### Using Task Scheduler to configure cron job

Here is how I successfully configured Task Scheduler to work:

1. Open Control Panel
2. Go into Task Scheduler
3. Click Create > Scheduled Task > User-defined script
4. Enter a name for the task (i.e., NextCloud)
5. Set the user as root. Yes, I'm aware that cron.php has to be run as the same owner as config.php.
6. On the Schedule tab, choose the option for Daily and frequency for every 5 minutes
7. Under Task Settings, you will need to enter the following string making sure you modify according to your location:
   `cd /volume1/web/nextcloud; sudo -H -u http bash -c 'php80 -f cron.php'`
     ![alt text](https://bitbucket.org/ebell451/notes/raw/61c7813fd9228cd8d46da4972e7186f159c359f2/docs/images/synology_task-nextcloud-cron.png "Screenshot showing the script used in Synology Task Scheduler for Nextcloud cron job.")
8. Click Ok

### Caching

This resolves both the error in Settings and the execution of the cron.php via CLI.

![alt text](https://bitbucket.org/ebell451/notes/raw/a0ffcda2fc4ff04d4cab422b75bf44a8d812a8c3/docs/images/nc_on_syn-passed.png "Screenshot showing green checkmark on system checkup.")

It is highly recommended that you have memory cache configured for Nextclout and apparently the APCu module isn't available in the Synology PHP package as it is not in the list of modules in the Web Station interface. However, it is available in the default instance.

To activate it, create the file `usr/local/etc/php80/cli/conf.d/nextcloud.ini` with the following content:

```text
extension = apcu.so

[apc]
apc.shm_size = 512M
apc.enable_cli = 1
```

You may need to `chmod 644 nextcloud` and then enable the memcache normall in the Netcloud config.php. You do this by opening the Nextcloud config file and add the following line:

`'memcache.local' => '\\OC\\Memcace\\APcu',`

To verify the new ini file has been loaded, you can execute `php80 --ini`.

![alt text](https://bitbucket.org/ebell451/notes/raw/a0ffcda2fc4ff04d4cab422b75bf44a8d812a8c3/docs/images/nc_on_syn-php-ini.png "Screenshot showing php80 --ini results.")

## Active Backup for Business - Hyper-V Management access

If you receive an error trying to connect Synology Active Backup for Business to your Hyper-V Manager, you are led to instructions that want you to enable WinRM on the Windows machiane. However, you receive the following error message trying to enable WinRM:

``` txt
Error number:  -2144108183 0x80338169
WinRM firewall exception will not work since one of the network connection types on this machine is set to Public. Change the network connection type to either Domain or Private and try again.
```

This can be resolved by running the following PowerShell command...or at least it has worked numerous times for me:

`Enable-PSRemoting -SkipNetworkProfileCheck`

I need to use this command even when running `Get-NetConnectionProfile` showed that all my profiles were Private. In my setup, this is actually caused because I am using a dedicated network connection for Hyper-V that isn't "exposed" to the OS. This presents a situation where it isn't easy to change the NetworkCategory of that network connection.