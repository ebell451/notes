# Pulseway Notes

For a good while I used [Pulseway](https://www.pulseway.com/) as my RMM tool of choice. I had tried most of the ones available, but stuck with Pulseway because, at the time, it didn't seem to embrace the "all updated functionality will be in modules and cost extra" mentality. Here are some scripts and other notes collected along the way...yes most of these are commands that can be executed at Windows CLI:

## Branding, change the display name of, PC Monitor Services on Windows

I wanted to remove Pulseway's name and add my own.
`sc config "PC Monitor" DisplayName= "AlliedVOA RMM Agent"`

## SpeedTest

Speed test run via PowerShell script
`'{0:N2}' -f ((10/(Measure-Command {Invoke-WebRequest -usebasicparsing 'https://cdn.speedof.me/sample2048k.bin'|Out-Null}).TotalSeconds)*8)`

## Disk Errors

Reports whether or not disk errors are found in the logs.

```posh
if ((get-host).Version.Major -ge 4){
$XmlQuery = [xml]@'
<QueryList>
  <Query Id="0" Path="System">
    <Select Path="System">*[System[Provider[@Name='disk'] and TimeCreated[timediff(@SystemTime) <= 86400000]]]</Select>
  </Query>
</QueryList>
'@
$LogOutput = Get-WinEvent -FilterXml $XmlQuery -ErrorAction SilentlyContinue
}
  else{
    $LogOutput = Get-EventLog -LogName system -Source disk -After (get-date).AddDays(-1) -ErrorAction SilentlyContinue
    }

if ($LogOutput){
Write-Host "---ERROR---"
Write-Host "Disk messages in system log found"
$LogOutput | fl TimeGenerated, Message
exit 1010
}

else{
Write-Host "---OK---"
Write-Host "No disk messages in system log found"
exit 0
}
```

## Firewall On

Turn on firewall for ALL profiles.
`NetSh Advfirewall set allprofiles state on`

## Rename Group

This script modifies the registry to change the group name of selected systems

```posh
#Replace 'NewGroupNameHere' with whatever group name you would like to use.
#Example, $NewGroup = 'Servers'

$PreviousGroup = Get-ItemProperty -Path "HKLM:\Software\MMSOFT Design\PC Monitor\" -Name GroupName
$NewGroup = 'IC - Police Department'
Write-Host 'The Pulseway group is currently set to:' $PreviousGroup.GroupName
Write-Host 'The Pulseway group will be set to:' $NewGroup
Set-ItemProperty -Path "HKLM:\Software\MMSOFT Design\PC Monitor\" -Name GroupName -Value $NewGroup
Write-Host 'Verifying...'
$Verification = Get-ItemProperty -Path "HKLM:\Software\MMSOFT Design\PC Monitor\" -Name GroupName
Write-Host 'The Pulseway group was successfully changed to' $Verification.GroupName
```

## Client Backup

This was specific to one customer where we backed up files from a "server" to an external drive.

```cli
# Map Drive Letter
net use Z: \\server\clients /persistent:YES

# Backup Clients folder on server to external hard drive
xcopy \\server\clients\*.* D:\Backups\ /D /S /Y

# Backup local My Documents folder to external hard drive
xcopy "c:\users\uname\OneDrive\Documents\*.*" "D:\My Documents Backup\" /D /S /Y

# Backup local Desktop folder to external hard drive
xcopy "c:\users\uname\Desktop\*.*" "D:\Other\Desktop" /D /S /Y

# Backup local Favorites folder to external hard drive
xcopy "c:\users\uname\Favorites\*.*" "D:\Other\Favorites" /D /S /Y

$wshell = New-Object -ComObject Wscript.Shell
$wshell.Popup("The daily backup of uname's files has been completed.",0,"Backup Completed",0x1)
```

## Create Support Link Shortcut

This is a PowerShell script that creates a shortcut in the Public\Desktop directory providing a shortcut for all users on the machine.

```posh
$TargetApplication = "C:\Program Files\Pulseway\pcmontask.exe"
$TargetArguments = " support"
$ShortcutFile = "$env:Public\Desktop\Get Support.lnk"
$WScriptShell = New-Object -ComObject WScript.Shell
$Shortcut = $WScriptShell.CreateShortcut($ShortcutFile)
$Shortcut.TargetPath = $TargetApplication
$Shortcut.Arguments = $TargetArguments
$Shortcut.Save()
```
