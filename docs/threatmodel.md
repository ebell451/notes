# Threat Modeling

*The information contained herein was gleaned from the sources mentioned in the footnote section below*

Looking for design flaws with security implications. We threat model to anticipate problems when it is inexpensive to deal with them. Threat modeling falls under Threat Assessment in the Software Assurance Maturity Model.

## How?

By answering the following four questions:

1. What are we working on?
2. What can go wrong?
3. What are we going to do about it?
4. Did we do a good/acceptable job?

## 1. What are we working on?

### Data Flow Diagram[^1]

- Entity (External entities) - Rectangle
  - Examples: users, producers, clients, inputs, sensors
  - Destinations: other users, server systems, consumers, output, displays
- Processes - Oval
  - conceptual processes that act on flows of data in transit
  - an interception point, a function within your project, endpoints in an API
  - devices that store and forward (network switch)
- Data stores - Cylindar or Horizontal bars
  - where data is stored or at rest
  - **data stores connect only to processes and entities only interact with data stores through prcesses**
  - Label with what they store
- Data flows - Arrow
  - shows the direction inh which important data flows
  - each arrow should only flow in one direction, to show returning data, use a new arrow
- Trust Boundary (Zone) - Rectangle with dashed border
  - shows where transitions happen between zones of differing trust (any place where data is passed between processes or where user input is accepted)

### Examples of completed diagrams

Example 1:![Basic example of complete diagram](https://files.cdn.thinkific.com/file_uploads/439768/images/659/945/d36/Sketch-cropped.png?width=1920)

Example 2:![Example of complete diagram](https://res.cloudinary.com/hy4kyit2a/f_auto,fl_lossy,q_70/learn/modules/threat-modeling-fundamentals/create-a-threat-model/images/2a292ce0eb9553fab21194d83f0af066_9-ef-77-af-7-65-e-7-4691-a-111-b-989-e-549000-c.png)

### Ready to Move On?[^2]

- Are all processes and entities labeled?
- Did you accurately represent the system being analyzed?
- All trust boundaries defined?
- Does all data in the diagram eventually flow both from and to one or more entities?
- Do any flows go "nowhere" or outside of your project? If so, note the other scope or system as an entity.
- Have you added arrows to the data flows?
- Have you numbered interaction points?

## 2. What can go wrong?

### Use STRIDE

- Spoofing
- Tampering
- Repudiation
- Information disclosure
- Denial of service
- Elevation of privilege

| STRIDE mnemonic | Violates Property | Class of Threats | Example Threats | Mitigations |
| --- | --- | --- | --- | --- |
| **S**poofing | Authentication | An attacker pretends to be someone or something that they're not (impersonation) | Changing display names or identities, pretending to be a Nigerian prince, Donald Trunp, paypal.com, ntdll.dll | * Authentication and anomaly detection<br>* Security-first user experience design<br>* Authentication encryption to support antispoofing<br>* Antitampering and anti-information disclosure properties |
| **T**ampering | Integrity | An attacker can modify or delete data. | Intercepting and modifying a data - modifying a file; changing code in an NPM repo, or a packet as it traverses the network | * Data input validation<br>* Cryptographic signatures<br>* Error-detection codes; for example, hash-based message authentication code (HMAC) |
| **R**epudiation | Non-repudiation (Accountability) | An attacker can perform some action and deny it later because there is no record or evidence captured. | Deleting or editing records without a log entry; "I didn't send that email;" "I certainly didn't visit that web site, dear!" | * Database event logs<br>* Application event logs<br>* Authentication logs |
| **I**nformation disclosure | An attacker obtains access to data that should be protected or exposing information to someone not authorized to see it | Confidentiality | Exfiltrating data from a database; publishing a list of customers to a web site | * Encryption<br>* Network boundary enforcement<br>* Constraining information outputs |
| **D**enial of service (DoS) | Availability | An attacker does something to degrade a service or deny it to other entities. | Flooding a host with network data; routing packets to a black hole | * Flood control<br>* Governors (resource limiters)<br>* Monitoring<br>* Performance tuning or scaling<br>* Availability and reliability enhancements |
| **E**levation of privilege (EoP) | Authorization | An attacker figures out a way to do something or obtain access to something despite not having the permissions | Allowing a remote Internet user to run commands is the classic example; Finding hidden administrative user interfaces  | * Authorization<br>* Permissioning and anomaly detection<br>* Role-based access control<br>* Network access control lists<br>* Ensuring permissions are consistently applied across the entire system |

![Stride and Elements](https://sinnema313.files.wordpress.com/2020/09/stride-dfd-mapping.png)

## 3. What are we going to do about it?

1. Track your work - the one constant as there are a lot of variations to this answer

2. Score the threats using CVSS (Common Vulnerability Scoring System)
   - Software developer - write a feature
   - Operations deploy a control
   - Risk management
     - Accept
     - Transfer

3. Important that you do not ignore what was discovered during the process.

## 4. Did we do a good job?

- Did we answer each of the questions?
- Is there a diagram or some other model that can show what we've been working on?
- Has the model been gone through systematically?
- List of things we are going to do?
- Are we tracking and managing the items on the list?

**Would you recommend threat modeling to a colleague?**

---

## Threat Modeling Traps[^3]

| No. | Trap | Fix
| --- | --- | --- |
| 1 | Think like an attacker | Serious work is helped by structure
| 2 | You're never done Threat Modeling | Think of it as a process with goals
| 3 | The way to Threat Model is...too much focus on the how | Focus on helping people find good threats, different skills, systems
| 4 | Threat Modeling as one skill | There's technique & repertoire - bunch of little details grouped together
| 5 | Threat Modeling is easy | Plan to work, build muscle
| 6 | Threat Modeling is for specialists | Make it like version control - ev7ryone in technology understands it
| 7 | The wrong focus - "start from your assets or thinking about your attackers" | Remember #3 - you should focus on fixing the problems and not just finding the threats
| 8 | Straining against the supply chain - "trying to do it all" | Think about an alliance along your supply chain
| 9 | Laser like focus on threats | What has changed are not the threats, but the reqwuirements
| 10 | Threat modeling at the wrong time | Early and often

### Summary

- Anyone can threat model, and everyone should...soon!
- The skills, techniques and repertoire can all be learned
- There are many traps
- Threat modeling can be the most effective way to drive security through your product, service or system

### Notes

- Threats tend to follow the dataflow.
- Threat modeling is a set of technical activities. Allows us an opportunity to exercise our social interactions with others.

[^1]: [Secure Cloud Development - Data Flow Diagrams and Threat Models](https://remonsinnema.com/2020/09/21/data-flow-diagrams-and-threat-models/)
[^2]: [Salesforce Trailblazer Training - Threat Modeling Fundamentals](https://trailhead.salesforce.com/content/learn/modules/threat-modeling-fundamentals/spot-the-threats)
[^3]: [Adam Shostack](https://shostack.org/resources/videos)
