# How to Complete Your Master's Degree in One Semester at WGU

by Blake Curtis via [LinkedIn](https://www.linkedin.com/pulse/how-complete-your-masters-degree-2-months-western-wgu-curtis-m-s-/)

## Always take the pre-assessment first! - You don't know what you don't know

- Review Your Weaknesses
  - Make a QRG (Quick Reference Guide)
    - Look up definitions for each distractor and correct answer (key)
    - Research the wrong answers as well - it will help you understand what you need to focus on most in the course material
    - Go through the course material and study your weaknesses - be sure to answer the questions at the end of each section and add these to your QRG
  - Review course notecards after making your QRG
- Prepping for the OA (Objective Assessment)
  - To through each question in your QRG answering in your head
  - Know the wrong answers too - don't just focus on the right answer. Understand why the distractors are wrong too.
    - You'll actually learn the material and pass the exam
    - This will prepare you for similar exams and you will be able to easily identify wrong answers

## Take the OA

- schedule as soon as you feel "somewhat" confident
- you can literally take your assessment in 30 minutes at any time of the day

## Passing the "dreaded" PA (Performance Assessment)

they are assessing competency and ensuring you can articulate your thoughs on a particular medium in a succinct and clear manner

### Lead the Horse to Water

they will not take the time to pick through your paper to see if you met their assessment criteria

### Create your outline based on their grading rubric

- Break down the Rubric & Requirements into Actionable Tasks
  - Write down the requirement
  - Restate the Competent section into specific questions

### Understand the Assignment

#### review the scenario/overview - understand the

- problem
- proposed solution
- why the solution is appropriate

#### **SQ3R** - Survey - Question - Read - Recite - Review

- Survey = creating an outline of the content (first para & first sentence)
- Question = make sense out of the material (make quick bullet points of the main problems)
- Read = Optional? instead look for keywords to identify the core problems
  - wishes
  - wants
  - considering
  - fails
  - tries
  - etc.
- Recite = good for OA, but not for drafting a paper for your PA (PA's typically introduce a very specific issue that they want you solve)

### Gather your Resources/References

- WGU has a few good resources in the Course Information section of your assessment
- Course instructors may have additional resources
- Reddit
- IT Resources
- Social Media (LinkedIn)

### Draft the paper and stop procrastinating

- write against the grading rubric & requirements
- ask yourself the questions you created earlier

### Don't sweat the resubmissions

- overwhelm them with tons of legitimate material
- extremely concise - very straightforward and focused submissions
  - if you think more is needed - DON'T
  - the review and resubmission will help you understand exactly what is missing
  - you are writing your papers for them, not for you

## Conclusion- it will take time to complete

## Tips

Any classes that only have one type of assessment should be added to your current term - knock those out first **especially the assessment objectives**
