# Tips and Tricks for WinOS

## ~~Scoop~~ Shovel - Package Manager for Windows

Go back to the [homepage](index.md) to read more about ~~scoop~~ shovel. The reason for the switch to shovel is due to updates to the core. You can read additional information in Shovel's [GitHub Readme](https://github.com/Ash258/Scoop-Core).

## Docker

### Using Docker Engine

#### Installation

How to install the Docker Engine versus Docker Desktop on Windows 10/11. The ideal solution would be to install Docker Enterprise on Windows Server.

- PowerShell
  ```
  curl.exe -o docker.zip -LO https://download.docker.com/win/static/stable/x86_64/docker-20.10.12.zip
  Expand-Archive docker.zip -DestinationPath C:\
  [Environment]::SetEnvironmentVariable("Path", "$($env:path);C:\docker", [System.EnvironmentVariableTarget]::Machine)
  $env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine")
  dockerd --register-service
  Start-Service docker
  docker run hello-world
  ```

- Scoop - `scoop install main/docker`
- Chocolately - `choco install docker-engine`

#### If you have problems with Uninstalling Docker or removing C:\ProgramData\Docker

Take a look at the following [https://gist.github.com/ebell451/a316ab789f56ba624e27c12c6d45aa0c](https://gist.github.com/ebell451/a316ab789f56ba624e27c12c6d45aa0c)

#### Additional Installs

Two of the main complaints of using the Docker Engine versus Docker Desktop is the availability of *docker scan* and *docker compose*. You can manually install both of these services:

- Docker Scan: https://github.com/docker/scan-cli-plugin#install-docker-scan

- Docker-Compose: https://docs.docker.com/compose/install/ [choose the option for Windows Server even though you will be installing on Win 10/11]

- You will also have to install Snyk CLI manually - https://docs.snyk.io/features/snyk-cli/install-the-snyk-cli

## Virtual Desktops

Windows 10 introduced a feature called Virtual Desktops. Coming from macOS the virtual desktops feature/functionality in Windows 10 isn't as intuitive. Also, other than the Win+Tab there is no **hotkey** functionality to automatically, or easily, switch between virtual desktops.

To help resolve this issue there is the [Virtual Desktop Accessor](https://github.com/Ciantic/VirtualDesktopAccessor) on GitHub. This along with AutoHotKey allows you to create hotkeys to switch between virtual desktops. You will need to download a custom DLL.

Here is my modified script from the one provided on the GitHub page:

```AutoHotKey
DetectHiddenWindows, On
hwnd:=WinExist("ahk_pid " . DllCall("GetCurrentProcessId","Uint"))
hwnd+=0x1000<<32

; Define where you placed the dll file
hVirtualDesktopAccessor := DllCall("LoadLibrary", Str, "C:\Support\VirtualDesktopAccessor.dll", "Ptr")
GoToDesktopNumberProc := DllCall("GetProcAddress", Ptr, hVirtualDesktopAccessor, AStr, "GoToDesktopNumber", "Ptr")
GetCurrentDesktopNumberProc := DllCall("GetProcAddress", Ptr, hVirtualDesktopAccessor, AStr, "GetCurrentDesktopNumber", "Ptr")
IsWindowOnCurrentVirtualDesktopProc := DllCall("GetProcAddress", Ptr, hVirtualDesktopAccessor, AStr, "IsWindowOnCurrentVirtualDesktop", "Ptr")
MoveWindowToDesktopNumberProc := DllCall("GetProcAddress", Ptr, hVirtualDesktopAccessor, AStr, "MoveWindowToDesktopNumber", "Ptr")
RegisterPostMessageHookProc := DllCall("GetProcAddress", Ptr, hVirtualDesktopAccessor, AStr, "RegisterPostMessageHook", "Ptr")
UnregisterPostMessageHookProc := DllCall("GetProcAddress", Ptr, hVirtualDesktopAccessor, AStr, "UnregisterPostMessageHook", "Ptr")
IsPinnedWindowProc := DllCall("GetProcAddress", Ptr, hVirtualDesktopAccessor, AStr, "IsPinnedWindow", "Ptr")
RestartVirtualDesktopAccessorProc := DllCall("GetProcAddress", Ptr, hVirtualDesktopAccessor, AStr, "RestartVirtualDesktopAccessor", "Ptr")
; GetWindowDesktopNumberProc := DllCall("GetProcAddress", Ptr, hVirtualDesktopAccessor, AStr, "GetWindowDesktopNumber", "Ptr")
activeWindowByDesktop := {}

; Restart the virtual desktop accessor when Explorer.exe crashes, or restarts (e.g. when coming from fullscreen game)
explorerRestartMsg := DllCall("user32\RegisterWindowMessage", "Str", "TaskbarCreated")
OnMessage(explorerRestartMsg, "OnExplorerRestart")
OnExplorerRestart(wParam, lParam, msg, hwnd) {
    global RestartVirtualDesktopAccessorProc
    DllCall(RestartVirtualDesktopAccessorProc, UInt, result)
}

MoveCurrentWindowToDesktop(number) {
  global MoveWindowToDesktopNumberProc, GoToDesktopNumberProc, activeWindowByDesktop
  WinGet, activeHwnd, ID, A
  activeWindowByDesktop[number] := 0 ; Do not activate
  DllCall(MoveWindowToDesktopNumberProc, UInt, activeHwnd, UInt, number)
  DllCall(GoToDesktopNumberProc, UInt, number)
}

GoToPrevDesktop() {
  global GetCurrentDesktopNumberProc, GoToDesktopNumberProc
  current := DllCall(GetCurrentDesktopNumberProc, UInt)
  if (current = 0) {
    GoToDesktopNumber(7)
  } else {
    GoToDesktopNumber(current - 1)
  }
  return
}

GoToNextDesktop() {
  global GetCurrentDesktopNumberProc, GoToDesktopNumberProc
  current := DllCall(GetCurrentDesktopNumberProc, UInt)
  if (current = 7) {
    GoToDesktopNumber(0)
  } else {
    GoToDesktopNumber(current + 1)
  }
  return
}

GoToDesktopNumber(num) {
  global GetCurrentDesktopNumberProc, GoToDesktopNumberProc, IsPinnedWindowProc, activeWindowByDesktop

  ; Store the active window of old desktop, if it is not pinned
  WinGet, activeHwnd, ID, A
  current := DllCall(GetCurrentDesktopNumberProc, UInt)
  isPinned := DllCall(IsPinnedWindowProc, UInt, activeHwnd)
  if (isPinned == 0) {
    activeWindowByDesktop[current] := activeHwnd
  }

  ; Try to avoid flashing task bar buttons, deactivate the current window if it is not pinned
  if (isPinned != 1) {
    WinActivate, ahk_class Shell_TrayWnd
  }

  ; Change desktop
  DllCall(GoToDesktopNumberProc, Int, num)
  return
}

; Windows 10 desktop changes listener
DllCall(RegisterPostMessageHookProc, Int, hwnd, Int, 0x1400 + 30)
OnMessage(0x1400 + 30, "VWMess")
VWMess(wParam, lParam, msg, hwnd) {
  global IsWindowOnCurrentVirtualDesktopProc, IsPinnedWindowProc, activeWindowByDesktop

  desktopNumber := lParam + 1

  ; Try to restore active window from memory (if it's still on the desktop and is not pinned)
  WinGet, activeHwnd, ID, A
  isPinned := DllCall(IsPinnedWindowProc, UInt, activeHwnd)
  oldHwnd := activeWindowByDesktop[lParam]
  isOnDesktop := DllCall(IsWindowOnCurrentVirtualDesktopProc, UInt, oldHwnd, Int)
  if (isOnDesktop == 1 && isPinned != 1) {
    WinActivate, ahk_id %oldHwnd%
  }
}

; Switching desktops:
; Win + Ctrl + 1 = Switch to desktop 1
*#1::GoToDesktopNumber(0)

; Win + Ctrl + 2 = Switch to desktop 2
*#2::GoToDesktopNumber(1)

; Moving windowes:
; Win + Shift + 1 = Move current window to desktop 2, and go there
+#2::MoveCurrentWindowToDesktop(1)


; These lines were added to provide function that resembles macOS
; Ctrl + Number to switch to corresponding desktop
; You can also use Ctrl + Left/Right Arrow key(s) to toggle
^1::GoToDesktopNumber(0)
^2::GoToDesktopNumber(1)
^3::GoToDesktopNumber(2)
^4::GoToDesktopNumber(3)
^Left::GoToPrevDesktop()
^Right::GoToNextDesktop()
```

## AutoHotKey

[AutoHotKey](https://www.autohotkey.com/) is a free macro-creation and automation-oriented scripting utility for Windows. It is [OpenSource](https://github.com/Lexikos/AutoHotkey_L). Their website describes the application as *the ultimate scripting language for Windows*.

VSCodium has two helpful extensions when scripting with AutoHotKey:

- AutoHotKey: This extensions performs highlighting, etc.
- AutoHotKeyManager: This extensions adds additional functionality allowing you compile and run scripts with VSCodium and even has a Script Manager and *Scripts Memories* function.

## Reset Internet Connection from CLI

Perform an Internet connection reset via Command Prompt. To do this, follow the steps below:

1. Press Windows + R to open the Run window.
2. Type cmd, then press Ctrl+Shift+Enter (this allows you to run command as Administrator).
3. Enter the following commands:

     ```cmd
     netsh int ip reset resettcpip.txt
     netsh winhttp reset proxy
     netsh int ip reset
     ipconfig /release
     ipconfig /renew
     ipconfig /flushdns
     netsh winsock reset
     ```

4. Restart your PC to complete the process.

## GodMode

Windows Master Control Panel. Here is how to enter God Mode in Windows 10

1. Make sure your Microsoft system account has administrator privileges.
2. Right-click on the Windows 10 desktop and “Create a new folder.”
3. Right-click the the new folder and rename the folder `GodMode.{ED7BA470-8E54-465E-825C-99712043E01C}` press enter

## Rename DC

1. *Getting ready* - Open a command prompt. (Windows key+r (run) + cmd)
2. *Adding an alternate computer name* - `netdom computername <currentDC FQDN> /add:<newDCName FQDN>`
  At the command prompt, type `netdom computername wrongname.domain.local /add:server.domain.local` This should return with "Added (NAME) as an alternate name for the computer. The command completed successfully.
3. *Make the new name the primary* - `netdom computername <currentDC FQDN> /makeprimary:<newDCName FQDN>`
  Type `netdom computername wrongname.domain.local /makeprimary:server.domain.local`
  **IMPORTANT**: This command will return successful, and warn you that you need to reboot immediately, as it may not authenticate logons (very important if only DC in forest)
4. *Reboot the server* - `shutdown /r /t 0`
5. *Check new server name* - Go to system properties and confirm new computer name.
6. *Remove old hostname* - `netdom computername <newDCName FQDN> /remove:<oldDCName FQDN>`
   `netdom computername server.domain.local /remove:wrongname.domain.local` Command should return successfully.
7. *Paranoia* - Type `dcdiag` to make sure everything is OK.
8. *Clean up* - If you use logon scripts, remember to update the UNC paths with the new server name.

## Delete profiles from registry

Open regedit and browse to to file path:  `/HKEY_LOCAL_MACHINE/SOFTWARE/MICROSOFT/WINDOWS NT/CURRENT VERSION/PROFILE LIST` and locate the appropriate profile name and delete all.

## Enable/Disable USB via CLI

You can use batch which gives you a couple of options.

1. Edit the registry key to disable usb devices from being used `reg add HKLM\SYSTEM\CurrentControlSet\Services\UsbStor /v "Start" /t REG_DWORD /d "4" /f` to enable change the value to **3**.
2. Or you can deny access to the files Usbstor.pnf and Usbstor.inf

```cli
cacls %windir%\Inf\Usbstor.pnf /d <user>
cacls %windir%\Inf\Usbstor.inf /d <user>
```

To enable use

```cli
cacls %windir%\Inf\Usbstor.pnf /p <user>:R
cacls %windir%\Inf\Usbstor.inf /p <user>:R
```

## Automatically boot into Windows as an administrator

Add the appropriate information to the following registry keys located at `HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Windows NT\CurrentVersion\Winlogon`

1. “DefaultDomainName”=””
2. “DefaultUserName”=””
3. “DefaultPassword”=””

## Complete uninstall SCCM

1. Go to C:\WINNT\system32\ccmsetup\{4CD82FBB-0AFC-4864-A089-15364DF5F14B}
   1. Right Click on client.msi
   2. Click “Uninstall”
2. Reboot machine
3. Delete the following directories:
   1. C:\WINNT\system32\ccmsetup
   2. C:\WINNT\system32\ccm
4. Delete the following registry keys:
   1. HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\CCM
   2. HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\CCMSetup
   3. HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\SMS
5. Reinstall SCCM
6. Reboot machine

## Remotely reboot a machine

`shutdown /m \\machine_name /r /t 0`

## Enable/Disable remote access on a machine

1. Enable Terminal Server Services on the workstation
   1. HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Control\TerminalServer
   2. “fDenyTSConnections”=dword:
      1. 0 = disable
      2. 1 = enable
2. Open Remote Desktop Connection and enter name of machine

## Reset Edge

This may not work in the latest version of Win10

`remove-item $env:localappdata\Packages\Microsoft.MicrosoftEdge_8wekyb3d8bbwe\* -recurse -Force 2>$null`

`Get-AppXPackage -Name Microsoft.MicrosoftEdge | Foreach {Add-AppxPackage -DisableDevelopmentMode -Register "$($_.InstallLocation)\AppXManifest.xml" -Verbose}`

## Get Information from CLI

Startup Items

```Powershell
Get-CimInstance Win32_StartupCommand | Select-Object Name, command, Location, User | Format-List | Out-File $env:userprofile\Desktop\Startup-Items.txt
```

Get computer information

```Powershell
Get-ComputerInfo | Out-File $env:userprofile\Desktop\Info.txt
```

## Wipe ALL event logs

`powershell -command "    wevtutil el | Foreach-Object {Write-Host "Clearing $_"; wevtutil cl "$_"}     "`

## Set Time Limits for Any Account on Windows via CLI

`net user <username> /times:<days,times>` \
Set 7:00 am - 11:00 pm every weekday: \
`net user <username> /time:M-F,7am-11pm` \
Set 7:00 am - 11:00 pm every day: \
`net user <username. /time:Su-Sa,7am-11pm` \
Reset to default / turn off: \
`net user <username> /time:all`

## Microsoft Parental Controls not working

delete the contents of C:\ProgramData\Microsoft\Windows\Parental Controls\

## Sign PowerShell Script w/self-signed cert

### Create self-signed code-signing certificate

```Powershell
$subject = "Ethan Bell”
$cert = New-SelfSignedCertificate -Subject $subject -Type CodeSigningCert -CertStoreLocation cert:\LocalMachine\Root
```

### Signing Script or commands

```Powershell
#Variables
$FileName = Read-Host 'What is the filename of the script you would like to sign?'
$cert = Get-ChildItem -Path Cert:\LocalMachine\Root -CodeSigningCert

#Sign our PowerShell script
Set-AuthenticodeSignature -Certificate:$cert -FilePath $FileName
```
