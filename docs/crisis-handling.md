---
note:
    createdAt: 2020-06-27T07:27:31.807Z
    modifiedAt: 2020-06-28T03:57:24.642Z
    tags: []
---
# How to Communicate During the Six Stages of a Crisis

## 3/10/2009 - notes taken from a course

1. Warning / Watch Phase
2. Risk Assessment
3. Response
4. Management
5. Resolution - All Clear Signals / Deactivate
6. Recovery - Getting back to normal operations

> There are many models.

A crisis evolves and is not single-phase or one dimensional.

Comprehensive Crisis Communication Plan (3C Plan) adapts with the crisis cycle.

The stages of a crisis negatively affects cognitive processes.

- Be sure you are accurate, consistent and reinforce each other
- Balance ideas, information and words in the context of a crisis
- Avoid mixed or erroneous messages.

Stress negatively affects comphrension

- Simplify the message
- Reduce number of message points to three (3)
- Short sentences, check your numbers carefully, use pictures and graphics

3-3-30\
(\* 3 points, 3 sentences, 30 words \*)

## Handling Rumors

- Actively manage them, aggressive plan for dealing with this.
- Think through what you are saying - see it through your audience's eyes.
- Be open and honest.
- THINK about what you know that everyone else should know.
