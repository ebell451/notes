# Dynamic Tech Training

## Introduction to DAST and how Micro Focus Fortify fulfills the need

- [What is DAST](https://youtu.be/6okVFkDKORg) - [Older 2015 Video](https://www.youtube.com/watch?v=bmIQlm9_3Po)
- [Fortify WebInspect as the Solution](https://www.youtube.com/watch?v=zzrbNK-FzJY)

## Training - Watch customer facing training videos on Micro Focus Education website

- [WebInspect Foundations - Interactive Training](https://inter.viewcentral.com/events/cust/search_results.aspx?keyword=+WebInspect+Foundations+Interactive+ART+Training&cat1_id=&event_type_id=&event_address_id=&cbClass=&postingForm=default.aspx&cid=microfocus&pid=1&lid=1&cart_currency_code=&payment_type=&orderby_location=&orderby_date=&newRegistration=&bundle_location_group=&errmsg=)
- [WebInspect Foundations - eLearning](https://inter.viewcentral.com/events/cust/search_results.aspx?keyword=FT3E0029+WebInspect+Foundations&cat1_id=&event_type_id=&event_address_id=&cbClass=&postingForm=default.aspx&cid=microfocus&pid=1&lid=1&cart_currency_code=&payment_type=&orderby_location=&orderby_date=&newRegistration=&bundle_location_group=&errmsg=)

---

## Setup Lab

Minimum of three WinOS Servers.

- MSSQL Server
- WIE
- WI

---

## Installation of WIE (Standalone or Attached to existing SSC instance)

### Review the following

- Documents
  - Review the [installation guide](https://www.microfocus.com/documentation/fortify-webinspect-enterprise/)
- Guides & Presentations
  - [WebInspect Enterprise Setup](https://microfocusinternational.sharepoint.com/:p:/t/FortifyProj/EQRb1RivYj5OsncDq595pOcBIoF_TFvZm9_IrxPn6O_ZiA?e=rpVO6r) PowerPoint presentation providing an overview of how to install WIE and how it fits in with SSC
  - WebInspect Enterprise Installation Guide
  - [WIE Introduction](https://microfocusinternational.sharepoint.com/:p:/t/FortifyProj/EdGaOPcyIXlKjJSZRiXwW2sBG_lEfkTmhzzf7opZ-yDuNQ?e=YrKuwF)
  - [WIE-Presentation](https://microfocusinternational.sharepoint.com/:p:/t/FortifyProj/EQgsTQf5nSJBsl0twFoJlyABmaKzFhRRX71P87ImCqmbMg?e=7TGigV)
- Videos
  - WIE Setup Troubleshooting
    - Watch [WIE08 Video - Installation Overview](https://web.microsoftstream.com/video/deb4d25a-0409-40cb-a123-92af4458afad)
  - WIE Usage
    - [WIE01 - Introduction](https://web.microsoftstream.com/video/81537c40-2e59-41a0-82f2-adafdc7e8816)
    - [WIE02 - Use Case - High Level Overview](https://web.microsoftstream.com/video/cfd9ad8a-8536-4143-9951-9f1b1be775c3)
    - [WIE03 - Use Case - Managing Sensors](https://web.microsoftstream.com/video/24f26e18-64e3-4114-9599-3ad827eb38f9)
    - [WIE04 - Use Case - Self Service Scans](https://web.microsoftstream.com/video/24f26e18-64e3-4114-9599-3ad827eb38f9)
    - [WIE05 - Use Case - SSC Integration](https://web.microsoftstream.com/video/c9d33ba1-c602-4dcd-8970-33b714e9d77c)
    - [WIE06 - Use Case - Central Repository](https://web.microsoftstream.com/video/82d8d0c9-cac0-4d44-a1b5-823d65b4260e)
    - [WIE07 - Use Case - WIE & SSC Background Tasks](https://web.microsoftstream.com/video/577e0cce-e0a9-40a8-8ef6-70155ad210cf)
    - [WIE09 - Basic Config](https://web.microsoftstream.com/video/f9bca6b0-895c-4476-a6c8-05babb08d22c)
    - [WIE10 - Troubleshooting Demo + Q&A](https://web.microsoftstream.com/video/1d44cf2e-212d-471e-a395-3d61709a4eaf)

### WIE Tasks

1. Configure IIS
2. Add WIE User
3. Download and Install WIE
4. Install Enterprise Console
5. Install/Configure Sensor
   1. Activate Sensor
6. Setup default role in Enterprise Console

### WIE Exercises

- Configure at least two WIE Sensors
- Create a project in SSC and request a dynamic scan
  - See Fortify Software Security Center User Guide pages 85-90
- Create a Guided Scan of the <http://zero.webappsecurity.com> site
- Check the results published in WIE Web Console and compre with SSC console

---

## Installation of WI

### Review the following for WI

- Documents
  - Review the [installation guide](https://www.microfocus.com/documentation/fortify-webinspect/)
- Guides & Presentations
  - [WebInspect Setup Troubleshooting](https://microfocusinternational.sharepoint.com/:p:/t/FortifyProj/ERvOJ2_e8A1Bpb_1tXdMDfYBqE9ZcgY6ln59O6CdrFWX2g?e=VGziOT)
- Videos
  - WI Usage
    - [WI03 Guided Scan Overview](https://www.youtube.com/watch?v=WJ42mNNsDiI)
    - [WI04 Guided Scan Tips](https://www.youtube.com/watch?v=CarCXwwgAO4)
    - [WI05 Basic Scan Overview](https://www.youtube.com/watch?v=I7SpJ-uDPPY)
    - [WI06 Basic Scan Tips](https://www.youtube.com/watch?v=nC7UmTFXfbI)
    - [WI07 Enterprise Scans](https://www.youtube.com/watch?v=PM01mc7HhwE)
    - [WI08 Web Service Scans](https://www.youtube.com/watch?v=Q7UAgptnjGw)
  - WI Login Macros and Advanced Settings
    - [WI09 Login Macros](https://www.youtube.com/watch?v=aiZ-s_QArMQ)
    - [WI10 Advanced Settings](https://www.youtube.com/watch?v=2Vs3-VKAqj4)

### WI Tasks

1. Install SQLExpress
2. Install WI
3. Discuss Licensing
   1. Named User
   2. Concurrent User (Need LIM)
4. Locate the following WI file directories - browse to and become familiar with the following:
   1. %localappdata%\HP\HP WebInspect
   2. C:\ProgramData\HP\HP WebInspect
   3. C:\Program Files\Fortify\Fortify WebInspect
5. Connect to WIE Instance
6. Review Chapter 18 of the WebInspect Tools Guide
   1. Record a login macro for <http://zero.webappsecurity.com>
   2. Review Chapters 6-10 of the WI User Guide

### WI Exercises

1. Create a basic scan of the Zero <http://zero.webappsecurity.com> website where you only scan the starting directory.
2. Create a crawl only scan of the Zero website for both the starting directory and subdirectories.
3. Create a scan for the directory and subdirectories of Zero using the Standard policy.
4. Create a scan for the directory and subdirectories of Zero with a Login Macro using the Standard policy.
5. Create a scan for the directory and subdirectories of Zero with a Login Macro using the SQL Injection policy.
   - After the scan completes, individually retest some of the SQLI vulns.
6. Perform an authenticated, crawl only scan of <http://legacy.webappsecurity.com> site
   - Restrict to directory and subdirectories
   - User: user
   - Password: user
   - When the crawl is complete, start an audit for the completed crawl
   - Use the latest _DISA STIG_ Policy
7. Perform an authenticated, crawl and audi scan of <http://www.altoromutual.com>
   - Restrict to directory and subdirectories
   - User: __jsmith__
   - Password: __Demo1234__
   - Disable _Auto fill web forms_
   - Use the _OWASP Top 10 2017_ Policy
   - When complete, Rescan > Scan Again, but enable _Auto fill web forms_
   - Compare the results of the two scans

### WI Debugging Tools

Watch the following videos

- [WD01 - Introduction](https://www.youtube.com/watch?v=m1Wom2NvH90)
- [WD02 Traffic Monitor](https://www.youtube.com/watch?v=BLKqxJ4fEGc)
- [WD03 Web Proxy](https://www.youtube.com/watch?v=oGJUmh0BtGk)
- [WD04 WebInspect UI](https://www.youtube.com/watch?v=k4LvgTeSrVc)

### WI Debugging Tools Tasks & Exercises

- Review the User Guide regarding Traffic Analysis (page 105)
- Monitor a scan again Zero using the Traffic Monitor and Web Proxy

---

## Setup/Configure LIM

LIM is free to use, but you must have a concurrent license.

- Review the [LIM Installation and Troubleshooting Guide](https://microfocusinternational.sharepoint.com/:b:/t/FortifyProj/ER7koHpEejNHnvrRYsMVQgMB7sDOR0hDlKEJ_3Xq0x7PYQ?e=cgaeSb) (PDF-older) provides LIM installation steps and troubleshooting of common issues

### LIM Tasks

1. Install LIM
2. Activate LIM (Recommended user record the LIM Activation Token)
   1. This activated token is associate with another LIM
      1. LIM Activation, release current
      2. Enter old LIM Activation token
         1. from backup
         2. contact support/licensing
3. Add product licenses

---

## Support Training

1. The importance of a scan file with logs and traffic over log files only.
2. Understand where log files are located
3. Difference and similarities between WI and WIE Sensors
4. Tracing a WIE scan
5. LIM & WI licensing versus WIE licensing
6. Common issues & Broken Scans Troubleshooting
   1. AntiVirus/AntiMalware
   2. API Scanning
   3. Ciphers/Protocols
   4. LMR Troubleshooting

### Support Training Review and Tasks

- Watch the following videos
  - [WD05 Broken Scans - Missing Pages](https://www.youtube.com/watch?v=oIIKzUF86QU)
  - [WD06 Broken Scans - No Vulns](https://www.youtube.com/watch?v=ljGy4etgd9o)
  - [WD07 Broken Scans - Short Scan with Few Findings](https://www.youtube.com/watch?v=mV-1t_-_Mu8)
  - [WD08 Broken Scans - Slow Scan #1](https://www.youtube.com/watch?v=H1qxx83I6ag)
  - [WD09 Broken Scans - Slow Scan #2](https://www.youtube.com/watch?v=4ZUxGKtnlbI)
  - [WD10 Broken Scans - Missing Pages](https://www.youtube.com/watch?v=i0Lgr5hh6vE)
- For each case in the following presentation, please study the issue and compose a response to the customer - Post Introduction WebInspect Training Tasks ([Post Introduction WebInspect Training Tasks]([PowerPoint](https://microfocusinternational.sharepoint.com/:p:/t/FortifyProj/ETWj29VtMdlDrkpD9bNPIw4BAqNIqZzT8PFf97VQ4oQR9A?e=8qhQx1))

---

## WI Agent

- Review
  - [Introduction to Runtime](https://microfocusinternational.sharepoint.com/:p:/t/FortifyProj/EepJelAS-lhKruCAngG1p5kBGtPsuQVOXDb6gGgTtyw8Pg?e=kfQfp6) presentation
  - [WebInspect Agent](https://microfocusinternational.sharepoint.com/:p:/t/FortifyProj/EekvmxiiTOlPmcBU3Ocg-zEBdAKLoKQkhnSRmssa1XDYAQ?e=6cgYZQ) presentation
- Exercise
  - Install a WI Agent on a Tomcat instance running WebGoad
  - Check the WI Agent logs to ensure proper installation and the agent is running
  - Conduct a scan of the WebGoat installation

---

## Deeper Dive Videos

Some deeper dive videos with Shawn Simpson are available on the [Fortify Support Team SharePoint](https://microfocusinternational.sharepoint.com/teams/FortifyProj/Lists/Dynamic%20Training%20Topics%20%20New/AllItems.aspx) site.
