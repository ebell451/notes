# Fortify WebInspect

## [Documentation](https://www.microfocus.com/documentation/fortify-webinspect/)

## [Fortify Community](https://community.microfocus.com/t5/Fortify/ct-p/fortify)

## [_Fortify Unplugged_ YouTube Channel](https://www.youtube.com/c/FortifyUnplugged)
